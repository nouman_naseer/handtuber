package abraham.graham.com.handtuber.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import org.json.JSONException;
import org.json.JSONObject;

import lombok.Getter;
import lombok.Setter;


//*********************************************************************
public class Bends
        implements Parcelable
//*********************************************************************
{
    @Getter @Setter private int _id;
    @Getter @Setter private String type;
    @Getter @Setter private String size;
    @Getter @Setter private String radius;
    @Getter @Setter private String measureUnit;
    @Getter @Setter private int bendType;
    @Getter @Setter private String imageSrc;
    @Getter @Setter private String name;
    @Getter @Setter private String filePath;
    @Getter @Setter private int defaultBender;
    @Getter @Setter private int section;
    @Getter @Setter private String sectionName;

    public static final int USER_SECTION = 100;
    public static final int APP_SECTION = 200;

    public static int BEND_APP_SPECIFIC = 1;
    public static int BEND_USER_SPECIFIC = BEND_APP_SPECIFIC + 1;
    public static final String BEND_TYPE_USER_SPECIFIC = "user_generated";
    public static final int BEND_DEFAULT = 1;
    public static final int BEND_NON_DEFAULT = 0;

    /*
         "type": "app_generated",
        "name": "Swagelok-HTB 1/2 Inch",
        "size": "0.5",
        "radius": "1.5",
        "unit": "in"
     */
    //*********************************************************************
    public Bends(JSONObject object, int type)
            throws JSONException
    //*********************************************************************
    {
        if (object.has("type"))
            setType(object.getString("type"));
        if (object.has("name"))
            setName(object.getString("name"));
        if (object.has("size"))
            setSize(object.getString("size"));
        if (object.has("radius"))
            setRadius(object.getString("radius"));
        if (object.has("unit"))
            setMeasureUnit(object.getString("unit"));
        if (object.has("path"))
            setFilePath(object.getString("path"));
        if (object.has("default"))
            setDefaultBender(object.getInt("default"));
        setBendType(type);

    }

    //*********************************************************************
    public Bends()
    //*********************************************************************
    {

    }
    //*********************************************************************
    public Bends(int section,String sectionName)
    //*********************************************************************
    {
        setSection(section);
        setSectionName(sectionName);
    }

    //*********************************************************************
    public void setMeasuringUnit(int unit)
    //*********************************************************************
    {
        if (unit == 0)
            setMeasureUnit("mm");
        else
            setMeasureUnit("in");
    }

    //*********************************************************************
    public int getMeasuringUnit()
    //*********************************************************************
    {
        if(TextUtils.isEmpty(getMeasureUnit()))
            return 0;

        if (getMeasureUnit().equals("mm"))
            return 0;
        else
            return 1;
    }

    //*********************************************************************
    @Override
    public int describeContents()
    //*********************************************************************
    {
        return 0;
    }

    //*********************************************************************
    public static final Parcelable.Creator<Bends> CREATOR = new Creator<Bends>()
            //*********************************************************************
    {

        //*********************************************************************
        @Override
        public Bends[] newArray(int size)
        //*********************************************************************
        {
            return new Bends[size];
        }

        //*********************************************************************
        @Override
        public Bends createFromParcel(Parcel source)
        //*********************************************************************
        {
            return new Bends(source);
        }
    };

    //*********************************************************************
    public Bends(Parcel in)
    //*********************************************************************
    {
        readFromParcel(in);
    }

    /*
     @Getter @Setter private int _id;
    @Getter @Setter private String type;
    @Getter @Setter private String size;
    @Getter @Setter private String radius;
    @Getter @Setter private String measureUnit;
    @Getter @Setter private boolean T;
    @Getter @Setter private int bendType;
    @Getter @Setter private String imageSrc;
    @Getter @Setter private String name;
    @Getter @Setter private String filePath;

     */
    //*********************************************************************
    private void readFromParcel(Parcel in)
    //*********************************************************************
    {
        setBendType(in.readInt());
        setName(in.readString());
        setSize(in.readString());
        setRadius(in.readString());
        setMeasureUnit(in.readString());
        setFilePath(in.readString());
        setType(in.readString());
        setDefaultBender(in.readInt());
        setSection(in.readInt());
        setSectionName(in.readString());
    }

    //*********************************************************************
    @Override
    public void writeToParcel(Parcel dest, int i)
    //*********************************************************************
    {
        dest.writeInt(getBendType());
        dest.writeString(getName());
        dest.writeString(getSize());
        dest.writeString(getRadius());
        dest.writeString(getMeasureUnit());
        dest.writeString(getFilePath());
        dest.writeString(getType());
        dest.writeInt(getDefaultBender());
        dest.writeInt(getSection());
        dest.writeString(getSectionName());
    }
}
