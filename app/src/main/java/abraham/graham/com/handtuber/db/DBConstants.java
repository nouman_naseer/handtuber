package abraham.graham.com.handtuber.db;

//*********************************************************************
public class DBConstants
//*********************************************************************
{

    public static String DATABASE_NAME = "hand_tuber_localdb";
    public static int DATABASE_VERSION = 1;
    public static String APP_VERSION = "1.0";
    public static String BEND_FETCH_LIMIT = "30";
    //Bender Table name & Column Names
    public static final String BENDERS_TABLE = "benders_table";
    public static final String BENDERS_COL_NAME = "name";
    public static final String BENDERS_COL_TYPE = "bend_type";
    public static final String BENDERS_COL_INPUT_VALUE = "input_value";
    public static final String BENDERS_COL_OUTPUT_VALUE = "output_value";

    public static final String DATABASE_DROP_BENDERS = "DROP TABLE IF EXISTS benders_table";
    public static final String DATABASE_CREATE_BENDERS_TABLE = "create table benders_table (_id integer primary key AUTOINCREMENT," +
            "name text  not null, bend_type integer not null,input_value text  not null, output_value text not null);";

    public static final String DATABASE_DROP_BENDS = "DROP TABLE IF EXISTS bends_table";
    /*
        /*
         "type": "app_generated",
        "name": "Swagelok-HTB 1/2 Inch",
        "size": "0.5",
        "radius": "1.5",
        "unit": "in"
     */
    public static final String DATABASE_CREATE_BENDS_TABLE = "create table bends_table(_id integer primary key AUTOINCREMENT, name text not null, bend_type integer not null," +
            "radius text not null, size text not null, unit text not null,path text, default_bender integer not null);";

    public static final String BENDS_TABLE = "bends_table";
    public static final String BENDS_COL_NAME = "name";
    public static final String BENDS_COL_TYPE = "bend_type";
    public static final String BENDS_COL_RADIUS = "radius";
    public static final String BENDS_COL_SIZE = "size";
    public static final String BENDS_COL_UNIT = "unit";
    public static final String BENDS_COL_PATH = "path";
    public static final String BENDS_COL_DEFAULT = "default_bender";


}
