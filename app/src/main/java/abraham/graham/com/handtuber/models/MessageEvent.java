package abraham.graham.com.handtuber.models;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by nouman on 3/20/17.
 */

//*********************************************************************
public class MessageEvent
//*********************************************************************
{
    public static final int UPDATE_BENDERS_TABLE=1;
    public static final int UPDATE_DEFAULT_FLAG=UPDATE_BENDERS_TABLE+1;
    @Getter @Setter private int operationType;

    //*********************************************************************
    public MessageEvent(int type)
    //*********************************************************************
    {
        this.operationType=type;
    }
}
