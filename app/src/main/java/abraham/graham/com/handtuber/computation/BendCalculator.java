package abraham.graham.com.handtuber.computation;

// ******************************************************************
public class BendCalculator
// ******************************************************************
{

    // ******************************************************************
    public static double getMeasureToSimpleOffset(double offset, double angle)
    // ******************************************************************
    {

        return Math.round(offset/Math.sin(Math.toRadians(angle)));
    }

    // ******************************************************************
    public static double getRunSimpleOffset(double offset,double angle)
    // ******************************************************************
    {
        return Math.round(offset * Math.tan(Math.toRadians(90 - angle)));
    }

    /**
     *
     * @param angle
     * @param benderRadius
     * 2*Bender Radius*TAN(RADIANS(Bend Angle/2))-(Bend Angle/360)*2*PI()*Radius)
     * @return
     */
    // ******************************************************************
    public static double getCalculatedGainPredictiveOffset(double angle,double benderRadius)
    // ******************************************************************
    {
        //2*Bender Radius*TAN(RADIANS(Bend Angle/2))-(Bend Angle/360)*2*PI()*Radius)
        return (2*benderRadius*Math.tan((Math.toRadians((angle/2))))-(angle/360)*2*Math.PI*benderRadius);
    }
    /**
     *
     * @param offset
     * @param bendAngle
     * Calculated Offset = (1/(SIN(RADIANS(Bend Angle)))*Offset)
     * @return
     */
    // ******************************************************************
    public static double getCalculatedOffsetPredictiveOffset(double offset,double bendAngle)
    // ******************************************************************
    {
        //Formula E7*TAN(RADIANS(90-E4))
        return offset*Math.tan(Math.toRadians(90-bendAngle));
    }

    /**
     *
     * @param calculatedGain
     * @param calculatedOffset
     * Measure To = Calculated Offset - Calculated Gain
     * @return
     */
    // ******************************************************************
    public static double getMeasureToPredictiveOffset(double calculatedGain,double calculatedOffset)
    // ******************************************************************
    {
        return calculatedOffset-calculatedGain;
    }


    /**
     * @param offset
     * @param angle
     * Offset*TAN(RADIANS(90-Angle))
     */
    // ******************************************************************
     public  static double getRunPredictiveOffset(double offset,double angle)
     // ******************************************************************
     {
         //Run = Offset*TAN(RADIANS(90-Angle))
         return offset*Math.tan(Math.toRadians(90-angle));
     }



}
