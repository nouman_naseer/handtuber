package abraham.graham.com.handtuber.Util;

//*********************************************************************
public enum Bends
//*********************************************************************
{
    SIMPLE_OFFSET(0),
    PREDICTIVE_OFFSET(1),
    PARALLEL_OFFSET(2),
    LONG_RUN_OFFSET(3),
    JOINT_OFFSET(4),
    FALL_CALCULATOR(5),
    BACKBEND_OFFSET(6);
    private final int value;

    //*********************************************************************
    Bends(final int newValue)
    //*********************************************************************
    {
        value = newValue;
    }

    //*********************************************************************
    public int getValue()
    //*********************************************************************
    {
        return value;
    }
}

