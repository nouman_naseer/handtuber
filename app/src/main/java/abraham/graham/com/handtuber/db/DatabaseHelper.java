package abraham.graham.com.handtuber.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

//*********************************************************************
public class DatabaseHelper extends SQLiteOpenHelper
//*********************************************************************
{
    //*********************************************************************
    public DatabaseHelper(Context context)
    //*********************************************************************
    {
        super(context, DBConstants.DATABASE_NAME, null,
                DBConstants.DATABASE_VERSION);
    }
    //*********************************************************************
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase)
    //*********************************************************************
    {
        try
        {
            sqLiteDatabase.execSQL(DBConstants.DATABASE_DROP_BENDERS);
            sqLiteDatabase.execSQL(DBConstants.DATABASE_CREATE_BENDERS_TABLE);
            sqLiteDatabase.execSQL(DBConstants.DATABASE_DROP_BENDS);
            sqLiteDatabase.execSQL(DBConstants.DATABASE_CREATE_BENDS_TABLE);
        }
        catch (Exception e)
        {
            System.out.println(e.getLocalizedMessage());
        }
    }

    //*********************************************************************
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion)
    //*********************************************************************
    {
        Log.w(DatabaseHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
    }
}
