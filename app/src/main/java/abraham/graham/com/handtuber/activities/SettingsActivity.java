package abraham.graham.com.handtuber.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.audiofx.BassBoost;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import abraham.graham.com.handtuber.R;
import abraham.graham.com.handtuber.Util.AndroidUtil;
import abraham.graham.com.handtuber.Util.Utils;
import lombok.val;


//*********************************************************************
public class SettingsActivity
        extends BaseActivity
        implements View.OnClickListener
//*********************************************************************
{
    private TextView mLabelSelectedUnit;
    private String[] mSelectedUnits = AndroidUtil.getStringArray(R.array.units_array);
    private int defaultUnit;
    private LinearLayout mUnitLayout;
    private LinearLayout mBenderLayout;
    private TextView mContactus;
    private TextView mFeedback;
    private TextView mLabelTutorials;
    //*********************************************************************
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    //*********************************************************************
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_layout);
        val toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initControls();
    }

    // ******************************************************************
    private void initControls()
    // ******************************************************************
    {
        mUnitLayout = (LinearLayout) findViewById(R.id.unit_layout);
        mLabelSelectedUnit = (TextView) findViewById(R.id.label_unit_selection);
        updateUnitControls();
        mUnitLayout.setOnClickListener(this);

        mBenderLayout = (LinearLayout) findViewById(R.id.bender_layout);
        mBenderLayout.setOnClickListener(this);

        mContactus = (TextView) findViewById(R.id.label_contact);
        mFeedback = (TextView) findViewById(R.id.label_feedback);
        mLabelTutorials =(TextView)findViewById(R.id.label_tutorial);
        mLabelTutorials.setOnClickListener(this);
        mContactus.setOnClickListener(this);
        mFeedback.setOnClickListener(this);
    }

    // ******************************************************************
    private void updateUnitControls()
    // ******************************************************************
    {
        defaultUnit = Utils.getDefaultUnit(this);
        if (defaultUnit <= mSelectedUnits.length)
            mLabelSelectedUnit.setText(mSelectedUnits[defaultUnit]);
    }

    // ******************************************************************
    @MainThread
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    // ******************************************************************
    {
        switch (item.getItemId())
        {
        case android.R.id.home:
            // go to previous screen when app icon in action bar is clicked
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // ******************************************************************
    private void displayUnitSelectionDialog()
    // ******************************************************************
    {
        //display dialog of the list of units + selected unit
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(SettingsActivity.this);
        // ******************************************************************
        alertDialog.setTitle(AndroidUtil.getString(R.string.selection_unit_dialog_title))
                   .setSingleChoiceItems(mSelectedUnits, defaultUnit,
                                         new DialogInterface.OnClickListener()
                                                 // ******************************************************************
                                         {
                                             @Override
                                             public void onClick(DialogInterface dialogInterface, int i)
                                             {
                                                 if (i != defaultUnit)
                                                     Utils.setDefaultUnit(i, SettingsActivity.this);
                                                 updateUnitControls();
                                             }

                                         })
                   .setPositiveButton(AndroidUtil.getString(R.string.selection_ok),
                                      new DialogInterface.OnClickListener()

                                      {
                                          @Override
                                          public void onClick(DialogInterface dialogInterface, int i)
                                          {
                                              dialogInterface.dismiss();
                                          }
                                      });

        final AlertDialog dialog = alertDialog.create();
        dialog.show();
    }

    // ******************************************************************
    private void moveToBenderListScreen()
    // ******************************************************************
    {
        startActivity(new Intent(this, BendersActivity.class));
    }

    // ******************************************************************
    private void sendEmail(String subject)
    // ******************************************************************
    {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", AndroidUtil.getString(R.string.feedback_email), null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
        startActivity(Intent.createChooser(emailIntent,
                                           AndroidUtil.getString(R.string.send_email_label)));
    }

    // ******************************************************************
    @Override
    public void onClick(View view)
    // ******************************************************************
    {
        switch (view.getId())
        {
        case R.id.bender_layout:
            moveToBenderListScreen();
            break;
        case R.id.unit_layout:
            displayUnitSelectionDialog();
            break;
        case R.id.label_contact:
            sendEmail(AndroidUtil.getString(R.string.contact_subject));
            break;
        case R.id.label_feedback:
            sendEmail(AndroidUtil.getString(R.string.feedback_subject));
            break;
        case R.id.label_tutorial:
            startActivity(new Intent(this, TutorialsActivity.class));
            break;
        }
    }
}
