package abraham.graham.com.handtuber.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.webkit.WebView;

import abraham.graham.com.handtuber.R;
import lombok.val;

/**
 * Created by nouman on 2/24/17.
 */

//*********************************************************************
public class InfoTutorialActivity
        extends BaseActivity
//*********************************************************************
{
    private WebView mWebView;
    public static final String INFO_TITLE = "INFO_TITLE";
    public static final String SCREEN_URL = "SCREEN_URL";

    private String mTitle;
    private String mUrl;


    //*********************************************************************
    private void getData()
    //*********************************************************************
    {
        Bundle args = getIntent().getExtras();
        if (args != null)
        {
            mTitle = args.getString(INFO_TITLE);
            mUrl = args.getString(SCREEN_URL);
        }
    }


    //*********************************************************************
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    //*********************************************************************
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info_layout);
        getData();
        val toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(mTitle);
        mWebView = (WebView) findViewById(R.id.web_view);
        mWebView.loadUrl(mUrl);

    }


}
