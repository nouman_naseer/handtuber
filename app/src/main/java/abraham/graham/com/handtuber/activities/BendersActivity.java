package abraham.graham.com.handtuber.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import abraham.graham.com.handtuber.R;
import abraham.graham.com.handtuber.Util.AndroidUtil;
import abraham.graham.com.handtuber.Util.UIResult;
import abraham.graham.com.handtuber.adapters.BendSectionAdapter;
import abraham.graham.com.handtuber.db.ConnectionTask;
import abraham.graham.com.handtuber.models.Bends;
import abraham.graham.com.handtuber.models.MessageEvent;
import lombok.val;

/**
 * Created by nouman on 1/14/17.
 */

//*********************************************************************
public class BendersActivity extends BaseActivity implements UIResult, Comparator<Bends>
//*********************************************************************
{

    private ListView mBendersList;
    private ArrayList<Bends> bendList;
    private BendSectionAdapter mAdapter;
    private FloatingActionButton btnNewBend;
    public static final String TAG="BendersActivity";
    //*********************************************************************
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    //*********************************************************************
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bender_activity_layout);
        val toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initControls();
        EventBus.getDefault().register(this);
    }


    //*********************************************************************
    private void initControls()
    //*********************************************************************
    {
        mBendersList = (ListView) findViewById(R.id.listView);
        btnNewBend = (FloatingActionButton) findViewById(R.id.fab);
        btnNewBend.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                newBendActivity();
            }
        });
        mBendersList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l)
            {
                Bundle bundle = new Bundle();
                val selectedBender = bendList.get(position);
                bundle.putParcelable(BendsDetailActivity.BENDERS_DISPLAY,selectedBender);
                newBendActivity(bundle);
            }
        });
        initBend();
    }

    //*********************************************************************
    @Override
    protected void onDestroy()
    //*********************************************************************
    {
        if (EventBus.getDefault() != null)
            EventBus.getDefault()
                    .unregister(this);
        super.onDestroy();
    }

    //*********************************************************************
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent event)
    //*********************************************************************
    {
        Log.d(TAG,"onMessageEvent");
        if (event.getOperationType() == MessageEvent.UPDATE_BENDERS_TABLE)
            initBend();
    }


    //*********************************************************************
    private void initBend()
    //*********************************************************************
    {
        Log.d(TAG,"initBend");
        ConnectionTask task = new ConnectionTask(BendersActivity.this, ConnectionTask.OT_FETCH_BENDERS, true, "Fetching Bends...");
        task.execute("");
    }

    //*********************************************************************
    private void newBendActivity(Bundle extras)
    //*********************************************************************
    {
        Intent bendIntent = new Intent(this, BendsDetailActivity.class);
        bendIntent.putExtras(extras);
        startActivity(bendIntent);
    }

    //*********************************************************************
    private void newBendActivity()
    //*********************************************************************
    {
        Intent bendIntent = new Intent(this, BendsDetailActivity.class);
        startActivity(bendIntent);
    }

    //*********************************************************************
    @Override
    public void updateResult(Object result)
    //*********************************************************************
    {
        if (result instanceof ArrayList<?>)
        {
            if (bendList != null && bendList.size() > 0)
            {
                Log.d("data","clearing data!");
                bendList.clear();
            }
            bendList = (ArrayList<Bends>) result;
            if (bendList != null && bendList.size() > 0)
            {
                //separate list here.
                ArrayList<Bends>appSpecific = getSeparatedList(Bends.BEND_APP_SPECIFIC,bendList);
                ArrayList<Bends>userSpecific = getSeparatedList(Bends.BEND_USER_SPECIFIC,bendList);
                bendList.clear();
                if (userSpecific != null && userSpecific.size() > 0)
                {
                    bendList.add(new Bends(Bends.USER_SECTION, AndroidUtil.getString(R.string.bender_user_section_name)));
                    Collections.sort(userSpecific,this);
                    bendList.addAll(userSpecific);
                }

                bendList.add(new Bends(Bends.APP_SECTION,AndroidUtil.getString(R.string.bender_app_section_name)));
                Collections.sort(appSpecific,this);
                bendList.addAll(appSpecific);
                mAdapter = new BendSectionAdapter(bendList, getLayoutInflater(), BendersActivity.this);
                mBendersList.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            }

        }
    }

    //*********************************************************************
    private ArrayList<Bends> getSeparatedList(int type,ArrayList<Bends>list)
    //*********************************************************************
    {

        ArrayList<Bends>processedList = new ArrayList<>();
        for (Bends bend:list)
        {
            if(bend.getBendType() == type)
                processedList.add(bend);
        }
        return processedList;
    }


    //*********************************************************************
    @Override
    public int compare(Bends o1, Bends o2)
    //*********************************************************************
    {
        return o1.getName().compareTo(o2.getName());
    }
}
