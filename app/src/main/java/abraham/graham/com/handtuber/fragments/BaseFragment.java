package abraham.graham.com.handtuber.fragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import abraham.graham.com.handtuber.R;


//*********************************************************************
public class BaseFragment extends Fragment
//*********************************************************************
{
    //*********************************************************************
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    //*********************************************************************
    {
        return inflater.inflate(R.layout.tabs, container, false);
    }

    //*********************************************************************
    public Drawable getFragmentIcon()
    //*********************************************************************
    {
        return null;
    }


}
