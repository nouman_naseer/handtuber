package abraham.graham.com.handtuber;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import abraham.graham.com.handtuber.Util.AndroidUtil;
import abraham.graham.com.handtuber.Util.UIResult;
import abraham.graham.com.handtuber.activities.BaseActivity;
import abraham.graham.com.handtuber.activities.SettingsActivity;
import abraham.graham.com.handtuber.fragments.BaseFragment;
import abraham.graham.com.handtuber.fragments.BendsFragment;
import abraham.graham.com.handtuber.fragments.CalculatorFragment;
import abraham.graham.com.handtuber.fragments.HistoryFragment;
import abraham.graham.com.handtuber.models.BendersModel;
import lombok.val;

//*********************************************************************
public class HomeActivity extends BaseActivity implements UIResult
//*********************************************************************
{
    private ViewPagerAdapter mSectionsPagerAdapter;
    private Toolbar mToolbar;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;

    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private int[] tabIcons = {
            R.drawable.bend,
            R.drawable.history,
            R.drawable.calculator
    };

    //*********************************************************************
    @Override
    protected void onCreate(Bundle savedInstanceState)
    //*********************************************************************
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        mTabLayout = (TabLayout) findViewById(R.id.tabs);
        mSectionsPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());


        mViewPager = (ViewPager) findViewById(R.id.container);
        setupViewPager(mViewPager);

        mTabLayout = (TabLayout) findViewById(R.id.tabs);
        mTabLayout.setupWithViewPager(mViewPager);
        setupTabIcons();
        // mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
    }

    //*********************************************************************
    private void setupViewPager(ViewPager viewPager)
    //*********************************************************************
    {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new BendsFragment());
        adapter.addFragment(new HistoryFragment());
        adapter.addFragment(new CalculatorFragment());
        viewPager.setAdapter(adapter);
    }


    //*********************************************************************
    private void setupTabIcons()
    //*********************************************************************
    {
        mTabLayout.getTabAt(0).setIcon(tabIcons[0]);
        mTabLayout.getTabAt(1).setIcon(tabIcons[1]);
        mTabLayout.getTabAt(2).setIcon(tabIcons[2]);
    }

    //*********************************************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    //*********************************************************************
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    //*********************************************************************

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    //*********************************************************************
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            Intent settingIntent = new Intent(this, SettingsActivity.class);
            startActivity(settingIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    //*********************************************************************
    public class ViewPagerAdapter extends FragmentStatePagerAdapter
            //*********************************************************************
    {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        //*********************************************************************
        public ViewPagerAdapter(FragmentManager fm)
        //*********************************************************************
        {
            super(fm);
            String[]tabTitleList = AndroidUtil.getStringArray(R.array.tab_names);
            for (String title:tabTitleList)
            {
                mFragmentTitleList.add(title);
            }
        }

        //*********************************************************************
        @Override
        public Fragment getItem(int position)
        //*********************************************************************
        {
            return mFragmentList.get(position);
        }

        //*********************************************************************
        public void addFragment(Fragment fragment)
        //*********************************************************************
        {
            mFragmentList.add(fragment);
        }

        //*********************************************************************
        @Override
        public int getCount()
        //*********************************************************************
        {
            // Show 3 total pages.
            return mFragmentList.size();
        }

        //*********************************************************************
        @Override
        public CharSequence getPageTitle(int position)
        //*********************************************************************
        {
            return mFragmentTitleList.get(position);
        }
    }

    //*********************************************************************
    @Override
    public void updateResult(Object result)
    //*********************************************************************
    {
        if (result == null)
            return;
        if (result instanceof ArrayList<?>)
        {
            ArrayList<BendersModel>mBendList = (ArrayList<BendersModel>) result;
            if(mBendList!=null && mBendList.size() > 0)
            {
                Toast.makeText(this,"Data found! recordCount => "+mBendList.size(),Toast.LENGTH_SHORT).show();
            }
        }
    }
}
