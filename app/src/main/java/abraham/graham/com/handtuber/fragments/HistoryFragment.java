package abraham.graham.com.handtuber.fragments;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import java.util.ArrayList;

import abraham.graham.com.handtuber.R;
import abraham.graham.com.handtuber.activities.BackbendOffsetActivity;
import abraham.graham.com.handtuber.activities.BaseActivity;
import abraham.graham.com.handtuber.activities.FallCalculator;
import abraham.graham.com.handtuber.activities.JointOffsetActivity;
import abraham.graham.com.handtuber.activities.LongRunOffsetActivity;
import abraham.graham.com.handtuber.activities.ParallelOffsetActivity;
import abraham.graham.com.handtuber.activities.PredictiveOffsetActivity;
import abraham.graham.com.handtuber.activities.SimpleOffsetActivity;
import abraham.graham.com.handtuber.adapters.HistoryAdapter;
import abraham.graham.com.handtuber.db.PersistanceManager;
import abraham.graham.com.handtuber.models.BendersModel;
import lombok.val;

//*********************************************************************
public class HistoryFragment extends Fragment
//*********************************************************************
{
    private ListView mBendsListView;
    private ArrayList<BendersModel> mBendList;
    private final String TAG = "bend-fragment";
    private Cursor mBenderCursor;
    //*********************************************************************
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    //*********************************************************************
    {
        super.onCreate(savedInstanceState);
    }

    //*********************************************************************
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    //*********************************************************************
    {
        val view = inflater.inflate(R.layout.history_fragment_layout, container, false);
        mBendsListView = (ListView) view.findViewById(R.id.bender_list);
        initBendData();
        mBendsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l)
            {
                if (mBenderCursor == null || mBenderCursor.getCount() == 0)
                    return;
                mBenderCursor.moveToPosition(position);
                //load model and pass that to the detail screen.
                BendersModel model = BendersModel.getModel(mBenderCursor);
                moveToScreen(model);

            }
        });
        return view;
    }

    //*********************************************************************
    @Override
    public void onResume()
    //*********************************************************************
    {
        super.onResume();
        initBendData();
    }

    //*********************************************************************
    private void moveToScreen(BendersModel model)
    //*********************************************************************
    {
        Intent intent=null;
        switch (model.getBendType())
        {
            case 0:
                intent = new Intent(getActivity(), SimpleOffsetActivity.class);
                break;
            case 1:
                intent = new Intent(getActivity(), PredictiveOffsetActivity.class);
                break;
            case 2:
                intent = new Intent(getActivity(), ParallelOffsetActivity.class);
                break;
            case 3:
                intent = new Intent(getActivity(), LongRunOffsetActivity.class);
                break;
            case 4:
                intent = new Intent(getActivity(), JointOffsetActivity.class);
                break;
            case 5:
                intent = new Intent(getActivity(), FallCalculator.class);
                break;
            case 6:
                intent = new Intent(getActivity(), BackbendOffsetActivity.class);
                break;
        }

        if(intent==null)
            return;

        intent.putExtra(BaseActivity.INPUT_VALUE,model);
        startActivity(intent);
    }



    //*********************************************************************
    private void initBendData()
    //*********************************************************************
    {
        PersistanceManager manager = PersistanceManager.getInstanceOf();
        mBenderCursor = manager.fetchBenderCursor(getActivity());
        HistoryAdapter adapter = new HistoryAdapter(getActivity(),mBenderCursor);
        mBendsListView.setAdapter(adapter);

    }
}
