package abraham.graham.com.handtuber.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

import abraham.graham.com.handtuber.R;
import abraham.graham.com.handtuber.Util.Operators;
import abraham.graham.com.handtuber.components.NumericEditText;
import lombok.val;


//*********************************************************************
public class CalculatorFragment extends Fragment
//*********************************************************************
{
    public static final int NUMBER_EDIT_TEXT_MAX_LENGTH = 30;
    public static final String VALUE = "value_calculator";
    public static final String RESULT = "result_calculator";

    public static final String ZERO = "0";
    public static final String ZERO_ZERO = "00";
    public static final String POINT = ".";
    public static final String ZERO_ZERO_ZERO = "000";

    private DecimalFormat decimalFormat;
    //input
    private NumericEditText inputNumberText;
    private TextView developmentOperationInputText;

    //button operation
    private Button clearBtn;
    private Button dividerBtn;
    private Button multiplicationBtn;
    private Button deleteBtn;
    private Button subtractionBtn;
    private Button sumBtn;
    private Button equalBtn;
    //private Button submitBtn;

    //button numeric
    private Button pointBtn;
    private Button zeroBtn;
    private Button threeZeroBtn;
    private Button oneBtn;
    private Button towBtn;
    private Button threeBtn;
    private Button fourBtn;
    private Button fiveBtn;
    private Button sixBtn;
    private Button sevenBtn;
    private Button eightBtn;
    private Button nineBtn;

    //operations values
    private boolean clickArithmeticOperator;
    private boolean clickEqualOperator;
    private boolean clearInput;
    private Double firstValue;
    private Double secondsValue;
    private String operatorExecute = Operators.NONE;

    //*********************************************************************
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    //*********************************************************************
    {
        super.onCreate(savedInstanceState);
    }

    //*********************************************************************
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    //*********************************************************************
    {
        val view = inflater.inflate(R.layout.calculator_layout, container, false);
       initComponents(view);
        return view;
    }


    //*********************************************************************
    private void initComponents(View view)
    //*********************************************************************
    {
        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
        decimalFormatSymbols.setGroupingSeparator(',');
        decimalFormatSymbols.setDecimalSeparator('.');
        decimalFormat = new DecimalFormat("#,###,##0.00", decimalFormatSymbols);

        String value = TextUtils.isEmpty(getActivity().getIntent().getStringExtra(VALUE)) ? ZERO : getActivity().getIntent().getStringExtra(VALUE);

        developmentOperationInputText = (TextView)view. findViewById(R.id.developing_operation_inputText);

        inputNumberText = (NumericEditText) view.findViewById(R.id.number_inputText);

        inputNumberText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(NUMBER_EDIT_TEXT_MAX_LENGTH)});
        inputNumberText.setText(value);

        clearBtn = (Button) view.findViewById(R.id.clear_button);
        deleteBtn = (Button) view.findViewById(R.id.delete_button);
        equalBtn = (Button) view.findViewById(R.id.equal_button);

        dividerBtn = (Button) view.findViewById(R.id.divider_button);
        multiplicationBtn = (Button)view. findViewById(R.id.multiplication_button);
        subtractionBtn = (Button)view. findViewById(R.id.subtraction_button);
        sumBtn = (Button) view.findViewById(R.id.sum_button);

        pointBtn = (Button) view.findViewById(R.id.point_button);
        zeroBtn = (Button) view.findViewById(R.id.zero_button);
        threeZeroBtn = (Button) view.findViewById(R.id.three_zero_button);
        oneBtn = (Button) view.findViewById(R.id.one_button);
        towBtn = (Button) view.findViewById(R.id.tow_button);
        threeBtn = (Button)view. findViewById(R.id.three_button);
        fourBtn = (Button)view. findViewById(R.id.four_button);
        fiveBtn = (Button) view.findViewById(R.id.five_button);
        sixBtn = (Button) view.findViewById(R.id.six_button);
        sevenBtn = (Button)view. findViewById(R.id.seven_button);
        eightBtn = (Button)view. findViewById(R.id.eight_button);
        nineBtn = (Button)view. findViewById(R.id.nine_button);

        List<Button> arithmeticOperators = new ArrayList<>();
        arithmeticOperators.add(dividerBtn);
        arithmeticOperators.add(multiplicationBtn);
        arithmeticOperators.add(subtractionBtn);
        arithmeticOperators.add(sumBtn);

        List<Button> secondaryOperators = new ArrayList<>();
        secondaryOperators.add(clearBtn);
        secondaryOperators.add(deleteBtn);
        secondaryOperators.add(equalBtn);

        List<Button> numericOperators = new ArrayList<>();
        numericOperators.add(pointBtn);
        numericOperators.add(zeroBtn);
        numericOperators.add(threeZeroBtn);
        numericOperators.add(oneBtn);
        numericOperators.add(towBtn);
        numericOperators.add(threeBtn);
        numericOperators.add(fourBtn);
        numericOperators.add(fiveBtn);
        numericOperators.add(sixBtn);
        numericOperators.add(sevenBtn);
        numericOperators.add(eightBtn);
        numericOperators.add(nineBtn);

        setOnClickListenerBtn(arithmeticOperators, mOnOperatorBtnClickListener);
        setOnClickListenerBtn(secondaryOperators, mOnOperatorBtnClickListener);
        setOnClickListenerBtn(numericOperators, mOnNumberBtnClickListener);
    }

    //*********************************************************************
    private final View.OnClickListener mOnOperatorBtnClickListener = new View.OnClickListener()
    //*********************************************************************
    {
        public void onClick(View view)
        {
            if (view instanceof Button)

            {

                String value = ((Button) view).getText().toString();

                switch (value)
                {
                    case Operators.SUM:
                    case Operators.SUBTRACTION:
                    case Operators.MULTIPLICATION:
                    case Operators.DIVIDER: {
                        clickEqualOperator = false;
                        operatorExecute = value;

                        if (!clickArithmeticOperator) {
                            clickArithmeticOperator = true;
                            prepareOperation(false);
                        } else {
                            replaceOperator(value);
                        }
                        break;
                    }
                    case Operators.CLEAR: {
                        clear();
                        break;
                    }
                    case Operators.DELETE: {
                        removeLastNumber();
                        break;
                    }
                    case Operators.EQUAL:
                    case Operators.SUBMIT: {
                        if (operatorExecute.equals(Operators.NONE)) {
                            returnResultOperation();
                        } else {
                            prepareOperation(true);
                            clickEqualOperator = true;
                            clickArithmeticOperator = false;
                            firstValue = null;
                            secondsValue = null;
                        }
                        break;
                    }
                }
            }
        }
    };

    //*********************************************************************
    private final View.OnClickListener mOnNumberBtnClickListener = new View.OnClickListener()
            //*********************************************************************
    {
        public void onClick(View view) {
            if (view instanceof Button) {
                String value = ((Button) view).getText().toString();
                concatNumeric(value);

//                equalBtn.setVisibility(View.VISIBLE);
//                submitBtn.setVisibility(View.GONE);
                clickEqualOperator = false;
                clickArithmeticOperator = false;
            }
        }
    };

    //*********************************************************************
    private void prepareOperation(boolean isEqualExecute)
    //*********************************************************************
    {
        clearInput = true;

        if (isEqualExecute) {
            developmentOperationInputText.setText("");
        } else {
            concatDevelopingOperation(operatorExecute, inputNumberText.getText().toString(), false);
        }

        if (firstValue == null) {
            firstValue = Double.parseDouble(inputNumberText.getText().toString().replaceAll(",", ""));
        } else if (secondsValue == null) {
            secondsValue = Double.parseDouble(inputNumberText.getText().toString().replaceAll(",", ""));

            if (!clickEqualOperator) {
                executeOperation(operatorExecute);
            }
        }
    }

    //*********************************************************************
    private void executeOperation(String operator)
    //*********************************************************************
    {
        if (firstValue == null || secondsValue == null) {
            return;
        }

        double resultOperation = 0.0;

        switch (operator) {
            case Operators.SUM: {
                resultOperation = firstValue + secondsValue;
                break;
            }
            case Operators.SUBTRACTION: {
                resultOperation = firstValue - secondsValue;
                break;
            }
            case Operators.MULTIPLICATION: {
                resultOperation = firstValue * secondsValue;
                break;
            }
            case Operators.DIVIDER: {
                if (secondsValue > 0) {
                    resultOperation = firstValue / secondsValue;
                }
                break;
            }
        }

        inputNumberText.setText(formatValue(resultOperation));
        firstValue = resultOperation;
        secondsValue = null;
        operatorExecute = Operators.NONE;
    }

    //*********************************************************************
    private void concatNumeric(String value)
    //*********************************************************************
    {
        if (value == null || inputNumberText.getText() == null) {
            return;
        }

        String oldValue = inputNumberText.getText().toString();
        String newValue = clearInput || (oldValue.equals(ZERO) && !value.equals(POINT)) ? value : oldValue + value;
        newValue = oldValue.equals(ZERO) && value.equals(ZERO_ZERO_ZERO) ? oldValue : newValue;
        inputNumberText.setText(newValue);
        clearInput = false;
    }

    //*********************************************************************
    private void concatDevelopingOperation(String operator, String value, boolean clear)
    //*********************************************************************
    {
        boolean noValidCharacter = operator.equals(Operators.CLEAR) || operator.equals(Operators.DELETE) || operator.equals(Operators.EQUAL);

        if (!noValidCharacter) {
            String oldValue = clear ? "" : developmentOperationInputText.getText().toString();
            developmentOperationInputText.setText(String.format("%s %s %s", oldValue, value, operator));
        }
    }

    //*********************************************************************
    private void removeLastNumber()
    //*********************************************************************
    {
        String value = inputNumberText.getText().toString();

        if (TextUtils.isEmpty(value) || value.length() == 1) {
            inputNumberText.setText(ZERO);
            return;
        }

        inputNumberText.setText(value.substring(0, value.length() - 1));
    }

    //*********************************************************************
    private void clear()
    //*********************************************************************
    {
        firstValue = null;
        secondsValue = null;
        operatorExecute = Operators.NONE;

        developmentOperationInputText.setText("");
        inputNumberText.setText(ZERO);
    }

    //*********************************************************************
    private void setOnClickListenerBtn(List<Button> btns, View.OnClickListener onClickListener)
    //*********************************************************************
    {
        for (Button button : btns) {
            button.setOnClickListener(onClickListener);
        }
    }

    //*********************************************************************
    private void returnResultOperation()
    //*********************************************************************
    {

        String result = inputNumberText.getText().toString();
        Intent resultIntent = new Intent();
        resultIntent.putExtra(RESULT, result);
    }

    //*********************************************************************
    private void replaceOperator(String operator)
    //*********************************************************************
    {
        String operationValue = developmentOperationInputText.getText().toString();

        if (TextUtils.isEmpty(operationValue)) {
            return;
        }

        String oldOperator = operationValue.substring(operationValue.length() - 1, operationValue.length());

        if (oldOperator.equals(operator)) {
            return;
        }

        String operationNewValue = operationValue.substring(0, operationValue.length() - 2);
        concatDevelopingOperation(operator, operationNewValue, true);
    }

    //*********************************************************************
    private String formatValue(double value)
    //*********************************************************************
    {
        String valueStr = decimalFormat.format(value);

        String integerValue = valueStr.substring(0, valueStr.indexOf(POINT));
        String decimalValue = valueStr.substring(valueStr.indexOf(POINT) + 1, valueStr.length());

        if (decimalValue.equals(ZERO_ZERO) || decimalValue.equals(ZERO)) {
            return integerValue;
        }

        return valueStr;
    }
}
