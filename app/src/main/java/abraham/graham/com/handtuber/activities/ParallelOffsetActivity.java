package abraham.graham.com.handtuber.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.HashMap;

import abraham.graham.com.handtuber.HandTuberApplication;
import abraham.graham.com.handtuber.R;
import abraham.graham.com.handtuber.Util.AndroidUtil;
import abraham.graham.com.handtuber.Util.Bends;
import abraham.graham.com.handtuber.Util.Utils;
import abraham.graham.com.handtuber.db.ConnectionTask;
import abraham.graham.com.handtuber.models.BendersModel;
import lombok.val;

//*********************************************************************
public class ParallelOffsetActivity
        extends BaseActivity
//*********************************************************************
{

    EditText mBendAngle;
    EditText mRefLength;
    EditText mSpacing;
    TextView mLabelAngle;
    TextView mLabelRefLength;
    TextView mLabelLength;
    TextView mLabelSpacing;
    TextView mLabelMeasure;
    TextView mValueMeasure;
    Button mCalculate;


    public static final String TAG = "PARALLEL_OFFSET";
    public static final String INPUT_FIELDKEY_ANGLE = AndroidUtil.getString(
            R.string.label_bend_angle);
    public static final String INPUT_FIELDKEY_REF_LENGTH = AndroidUtil.getString(
            R.string.label_ref_length);
    public static final String INPUT_FIELDKEY_SPACING = AndroidUtil.getString(
            R.string.label_spacing);
    public static final String OUTPUT_FIELD_MEASURE = AndroidUtil.getString(
            R.string.label_measure_to);
    public static final String OUTPUT_FIELD_DIAGRAM_LENGTH = AndroidUtil.getString(
            R.string.label_diagram_length);

    //*********************************************************************
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    //*********************************************************************
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.parallel_offset_layout);
        val toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initControls();
    }

    // ******************************************************************
    private void initControls()
    // ******************************************************************
    {
        mBendAngle = (EditText) findViewById(R.id.input_angle);
        mRefLength = (EditText) findViewById(R.id.input_ref_length);
        mSpacing = (EditText) findViewById(R.id.input_spacing);

        mCalculate = (Button) findViewById(R.id.btn_cal);

        mCalculate.setOnClickListener(new View.OnClickListener()
        {
            // ******************************************************************
            @Override
            public void onClick(View view)
            // ******************************************************************
            {
                computeValues();
            }
        });

        mValueMeasure = (TextView) findViewById(R.id.label_measurement_value);
        mLabelRefLength = (TextView) findViewById(R.id.txt_diagram_ref_length);
        mLabelAngle = (TextView) findViewById(R.id.txt_diagram_angle);
        mLabelLength = (TextView) findViewById(R.id.txt_diagram_length);
        mLabelSpacing = (TextView) findViewById(R.id.txt_diagram_spacing);
        mLabelMeasure = (TextView) findViewById(R.id.txt_diagram_measure);
        populateData();
    }

    // ******************************************************************
    private void computeValues()
    // ******************************************************************
    {
        if (TextUtils.isEmpty(mBendAngle.getText()
                                        .toString()))
            return;

        if (TextUtils.isEmpty(mSpacing.getText()
                                      .toString()))
            return;

        if (TextUtils.isEmpty(mRefLength.getText()
                                        .toString()))
            return;

        double bendAngle = Double.parseDouble(mBendAngle.getText()
                                                        .toString());
        double spacing = Double.parseDouble(mSpacing.getText()
                                                    .toString());
        double refLength = Double.parseDouble(mRefLength.getText()
                                                        .toString());

        double addedLength = getAddedLength(bendAngle, spacing);
        mLabelLength.setText(Double.toString(addedLength));
        mLabelRefLength.setText(Double.toString(refLength));
        mLabelAngle.setText(Double.toString(bendAngle));
        mLabelSpacing.setText(Double.toString(spacing));
        double measureTo = getMeasureTo(addedLength, refLength);
        mLabelMeasure.setText(Double.toString(measureTo));
        mValueMeasure.setText(Double.toString(measureTo));
        isComputed = true;
    }

    // ******************************************************************
    private void populateData()
    // ******************************************************************
    {
        BendersModel model = getModel();

        if (model == null)
            return;

        model.setIOValues();
        mBendAngle.setText(model.getInputMap()
                                .get(INPUT_FIELDKEY_ANGLE));
        mLabelAngle.setText(model.getInputMap()
                                 .get(INPUT_FIELDKEY_ANGLE));

        mSpacing.setText(model.getInputMap()
                              .get(INPUT_FIELDKEY_SPACING));
        mLabelSpacing.setText(model.getInputMap()
                                   .get(INPUT_FIELDKEY_SPACING));

        mRefLength.setText(model.getInputMap()
                                .get(INPUT_FIELDKEY_REF_LENGTH));
        mLabelRefLength.setText(model.getInputMap()
                                     .get(INPUT_FIELDKEY_REF_LENGTH));

        mLabelLength.setText(model.getOutputMap()
                                  .get(OUTPUT_FIELD_DIAGRAM_LENGTH));
        mLabelMeasure.setText(model.getOutputMap()
                                   .get(OUTPUT_FIELD_MEASURE));
        mValueMeasure.setText(model.getOutputMap()
                                   .get(OUTPUT_FIELD_MEASURE));
    }


    // ******************************************************************
    @Override
    protected void saveData()
    // ******************************************************************
    {
        if (!isComputed || getModel() != null)
            return;

        BendersModel model = new BendersModel();
        model.setBenderName(AndroidUtil.getString(R.string.parallel_offset_screen_name));
        model.setBendType(Bends.PARALLEL_OFFSET.getValue());

        HashMap<String, String> inputMap = new HashMap<>(3);
        inputMap.put(INPUT_FIELDKEY_ANGLE, mBendAngle.getText()
                                                     .toString());
        inputMap.put(INPUT_FIELDKEY_REF_LENGTH, mRefLength.getText()
                                                          .toString());
        inputMap.put(INPUT_FIELDKEY_SPACING, mSpacing.getText()
                                                     .toString());
        model.setInputMap(inputMap);

        HashMap<String, String> outputMap = new HashMap<>(2);
        outputMap.put(OUTPUT_FIELD_DIAGRAM_LENGTH, mLabelLength.getText()
                                                               .toString());
        outputMap.put(OUTPUT_FIELD_MEASURE, mLabelMeasure.getText()
                                                         .toString());
        model.setOutputMap(outputMap);
        model.saveModel();
        ConnectionTask task = new ConnectionTask(this, ConnectionTask.OT_INSERT_BENDS);
        task.execute(model);

    }

    /**
     * @param angle
     * @param spacing
     * @return TAN(Angle/2)*Spacing
     */
    // ******************************************************************
    private double getAddedLength(double angle, double spacing)
    // ******************************************************************
    {
        //TAN(Angle/2)*Spacing
        return Utils.round(Math.round(Math.tan(Math.toRadians(angle) / 2) * spacing), 1);
    }

    /**
     * @param addedLength
     * @param refLength   return TAN(Angle/2)*Spacing+Ref Length
     */
    // ******************************************************************
    private double getMeasureTo(double addedLength, double refLength)
    // ******************************************************************
    {
        //TAN(Angle/2)*Spacing+Ref Length
        return Math.round(addedLength + refLength);
    }

    //*********************************************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    //*********************************************************************
    {
        // Inflate the menu; this adds items to the action bar if it is pre
        getMenuInflater().inflate(R.menu.menu_info, menu);
        return true;
    }

    // ******************************************************************
    @Override
    protected void InfoUrl()
    // ******************************************************************
    {
        val url = getTutorialPath(HandTuberApplication.instance()
                                                      .getBendInfoByName(AndroidUtil.getString(
                                                              R.string.parallel_offset_screen_name))
                                                      .getTutorialPath());
        Intent infoTutorialIntent = new Intent(this, InfoTutorialActivity.class);
        infoTutorialIntent.putExtra(InfoTutorialActivity.INFO_TITLE,
                                    AndroidUtil.getString(R.string.parallel_offset_screen_name));
        infoTutorialIntent.putExtra(InfoTutorialActivity.SCREEN_URL, url);
        startActivity(infoTutorialIntent);
    }
}
