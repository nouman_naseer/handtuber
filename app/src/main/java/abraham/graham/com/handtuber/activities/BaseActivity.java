package abraham.graham.com.handtuber.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import abraham.graham.com.handtuber.R;
import abraham.graham.com.handtuber.Util.AndroidUtil;
import abraham.graham.com.handtuber.Util.UIResult;
import abraham.graham.com.handtuber.models.BendersModel;


//*********************************************************************
public class BaseActivity
        extends AppCompatActivity
        implements UIResult
//*********************************************************************
{
    public static final int CAL_TYPE_SIMPLE_OFFSET = 1;
    protected final double BENDER_RADIUS = 38.100;
    protected boolean isComputed = false;
    public static final String INPUT_VALUE = "INPUT_VALUE";

    //*********************************************************************
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    //*********************************************************************
    {
        super.onCreate(savedInstanceState);
    }

    // ******************************************************************
    protected BendersModel getModel()
    // ******************************************************************
    {
        if (getIntent().getExtras() == null || !getIntent().getExtras()
                                                           .containsKey(INPUT_VALUE))
            return null;
        return (BendersModel) getIntent().getExtras()
                                         .get(INPUT_VALUE);
    }

    // ******************************************************************
    @MainThread
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    // ******************************************************************
    {
        switch (item.getItemId())
        {
        case android.R.id.home:
            // go to previous screen when app icon in action bar is clicked
            saveData();
            onBackPressed();
            return true;
        case R.id.action_info:
            // go to previous screen when app icon in action bar is clicked
            InfoUrl();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    // ******************************************************************
    protected void InfoUrl()
    // ******************************************************************
    {

    }

    // ******************************************************************
    protected void saveData()
    // ******************************************************************
    {

    }

    // ******************************************************************
    protected String getTutorialPath(String name)
    // ******************************************************************
    {
       return String.format(AndroidUtil.getString(R.string.tutorial_url_path),name);
    }
    // ******************************************************************
    @Override
    public void updateResult(Object result)
    // ******************************************************************
    {
        super.onBackPressed();
    }
}
