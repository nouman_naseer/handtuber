package abraham.graham.com.handtuber.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.HashMap;

import abraham.graham.com.handtuber.HandTuberApplication;
import abraham.graham.com.handtuber.R;
import abraham.graham.com.handtuber.Util.AndroidUtil;
import abraham.graham.com.handtuber.Util.Bends;
import abraham.graham.com.handtuber.Util.Utils;
import abraham.graham.com.handtuber.db.ConnectionTask;
import abraham.graham.com.handtuber.models.BendersModel;
import lombok.val;

//*********************************************************************
public class JointOffsetActivity extends BaseActivity
//*********************************************************************
{


    public static final String TAG = "JOINT_OFFSET";
    private TextView mLabelDiagramRefLength;
    private TextView mLabelDiagramAngle;
    private TextView mLabelDiagramOffset;
    private TextView mLabelDiagramAllowance;
    private TextView mLabelDiagramA;
    private TextView mLabelDiagramB;
    private TextView mLabelDiagramC;

    private EditText mAngle;
    private EditText mRefLength;
    private EditText mOffset;
    private EditText mAllowance;

    private Button mCal;

    /**
     * Input Fields
     */
    public static final String INPUT_FIELD_ANGLE = AndroidUtil.getString(R.string.label_angle);
    public static final String INPUT_FIELD_RUN_LENGTH = AndroidUtil.getString(R.string.label_run_length);
    public static final String INPUT_FIELD_OFFSET = AndroidUtil.getString(R.string.label_offset);
    public static final String INPUT_FIELD_ALLOWANCE = AndroidUtil.getString(R.string.label_allowance);

    /**
     * Output Fields
     */

    public static final String OUTPUT_FIELD_A = AndroidUtil.getString(R.string.label_a);
    public static final String OUTPUT_FIELD_B=AndroidUtil.getString(R.string.label_b);
    public static final String OUTPUT_FIELD_C=AndroidUtil.getString(R.string.label_c);
    //*********************************************************************
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    //*********************************************************************
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.joint_offset_layout);
        val toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initControls();
    }

    //*********************************************************************
    private void initControls()
    //*********************************************************************
    {
        mLabelDiagramRefLength = (TextView) findViewById(R.id.txt_diagram_ref_length);
        mLabelDiagramAngle = (TextView) findViewById(R.id.txt_diagram_angle);
        mLabelDiagramOffset = (TextView) findViewById(R.id.txt_diagram_offset);
        mLabelDiagramAllowance = (TextView) findViewById(R.id.txt_diagram_allowance);
        mLabelDiagramA = (TextView) findViewById(R.id.txt_diagram_a);
        mLabelDiagramB = (TextView) findViewById(R.id.txt_diagram_b);
        mLabelDiagramC = (TextView) findViewById(R.id.txt_diagram_c);

        mAngle = (EditText) findViewById(R.id.input_angle);
        mRefLength = (EditText) findViewById(R.id.input_ref_length);
        mOffset = (EditText) findViewById(R.id.input_offset);
        mAllowance = (EditText) findViewById(R.id.input_allowance);

        mCal = (Button) findViewById(R.id.btn_cal);

        mCal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                computeValues();
            }
        });

        populateData();
    }

    //*********************************************************************
    private void computeValues()
    //*********************************************************************
    {
        if (TextUtils.isEmpty(mAngle.getText().toString()))
            return;
        if (TextUtils.isEmpty(mRefLength.getText().toString()))
            return;
        if (TextUtils.isEmpty(mOffset.getText().toString()))
            return;
        if (TextUtils.isEmpty(mAllowance.getText().toString()))
            return;

        mLabelDiagramAngle.setText(mAngle.getText().toString());
        mLabelDiagramRefLength.setText(mRefLength.getText().toString());
        mLabelDiagramOffset.setText(mOffset.getText().toString());
        mLabelDiagramAllowance.setText(mAllowance.getText().toString());


        double angle = Double.parseDouble(mAngle.getText().toString());
        double refLength = Double.parseDouble(mRefLength.getText().toString());
        double offset =  Double.parseDouble(mOffset.getText().toString());
        double allowance = Double.parseDouble(mAllowance.getText().toString());

        double slopeLength = getSlopeLength(angle,offset);

        Log.d(TAG,"SlopeLength => "+slopeLength);
        double gain = getGain(Utils.getBenderRadius(),angle);
        Log.d(TAG,"gain => "+gain);

        double run = getRun(offset,angle);
        Log.d(TAG,"run => "+run);
        double shrink = getShrink(slopeLength,run);

        Log.d(TAG,"shrink => "+shrink);

        double lengthBC= getLengthBC(allowance,gain);
        Log.d(TAG,"lengthBC => "+lengthBC);

        double lengthAB=getLengthAB(slopeLength,gain);

        Log.d(TAG,"lengthAB => "+lengthAB);
        double lengthA = getLengthA(refLength,lengthBC,gain,run);

        Log.d(TAG,"lengthA => "+lengthA);

        mLabelDiagramA.setText(Double.toString(Math.round(lengthA)));

        double lengthB = getLengthB(lengthA,lengthAB);
        Log.d(TAG,"lengthB => "+lengthB);
        mLabelDiagramB.setText(Double.toString(Math.round(lengthB)));

        double lengthC = getLengthC(lengthA,lengthAB,lengthBC);
        Log.d(TAG,"lengthC => "+lengthC);
        mLabelDiagramC.setText(Double.toString(Math.round(lengthC)));
        isComputed = true;

    }

    // ******************************************************************
    private void populateData()
    // ******************************************************************
    {
        BendersModel model = getModel();
        if (model == null)
            return;

        model.setIOValues();
        mAngle.setText(model.getInputMap().get(INPUT_FIELD_ANGLE));
        mLabelDiagramAngle.setText(model.getInputMap().get(INPUT_FIELD_ANGLE));

        mRefLength.setText(model.getInputMap().get(INPUT_FIELD_RUN_LENGTH));
        mLabelDiagramRefLength.setText(model.getInputMap().get(INPUT_FIELD_RUN_LENGTH));

        mOffset.setText(model.getInputMap().get(INPUT_FIELD_OFFSET));
        mLabelDiagramOffset.setText(model.getInputMap().get(INPUT_FIELD_OFFSET));

        mAllowance.setText(model.getInputMap().get(INPUT_FIELD_ALLOWANCE));
        mLabelDiagramAllowance.setText(model.getInputMap().get(INPUT_FIELD_ALLOWANCE));

        mLabelDiagramA.setText(model.getOutputMap().get(OUTPUT_FIELD_A));
        mLabelDiagramB.setText(model.getOutputMap().get(OUTPUT_FIELD_B));
        mLabelDiagramC.setText(model.getOutputMap().get(OUTPUT_FIELD_C));

    }


    // ******************************************************************
    @Override
    protected void saveData()
    // ******************************************************************
    {
        if(!isComputed || getModel() != null)
            return;

        BendersModel model = new BendersModel();
        model.setBenderName(AndroidUtil.getString(R.string.joint_offset_screen_name));
        model.setBendType(Bends.JOINT_OFFSET.getValue());

        HashMap<String,String> inputMap = new HashMap<>(4);
        inputMap.put(INPUT_FIELD_ANGLE,mLabelDiagramAngle.getText().toString());
        inputMap.put(INPUT_FIELD_RUN_LENGTH,mLabelDiagramRefLength.getText().toString());
        inputMap.put(INPUT_FIELD_OFFSET,mLabelDiagramOffset.getText().toString());
        inputMap.put(INPUT_FIELD_ALLOWANCE,mLabelDiagramAllowance.getText().toString());
        model.setInputMap(inputMap);

        HashMap<String,String>outputMap = new HashMap<>(3);
        outputMap.put(OUTPUT_FIELD_A, mLabelDiagramA.getText().toString());
        outputMap.put(OUTPUT_FIELD_B, mLabelDiagramB.getText().toString());
        outputMap.put(OUTPUT_FIELD_C, mLabelDiagramC.getText().toString());
        model.setOutputMap(outputMap);
        model.saveModel();
        ConnectionTask task = new ConnectionTask(this,ConnectionTask.OT_INSERT_BENDS);
        task.execute(model);

    }
    /**
     * @param angle
     * @param offset
     * @return (1/(SIN(RADIANS(Bend Angle)))*Offset)
     */
    //*********************************************************************
    private double getSlopeLength(double angle, double offset)
    //*********************************************************************
    {
        return (1 / (Math.sin(Math.toRadians(angle))) * offset);
    }

    /**
     * @param radius
     * @param angle
     * @return 2*Bender Radius*TAN(RADIANS(Bend Angle/2))-(Bend Angle/360)*2*PI()*Radius)
     */
    //*********************************************************************
    private double getGain(double radius, double angle)
    //*********************************************************************
    {
        //     2*BenderRadius*TAN(RADIANS(Bend Angle/2))-(Bend Angle/360)*2*PI()*Radius)
        return 2 * radius * Math.tan(Math.toRadians(angle / 2)) - ((angle / 360) * 2 * Math.PI * radius);
    }

    /**
     * @param offset
     * @param angle
     * @return Offset*TAN(RADIANS(90-Bend Angle))
     */
    //*********************************************************************
    private double getRun(double offset, double angle)
    //*********************************************************************
    {
        return Math.round(offset * Math.tan(Math.toRadians(90 - angle)));
    }

    /**
     * @param slopeLength
     * @param run
     * @return Slope length – Run
     */
    //*********************************************************************
    private double getShrink(double slopeLength, double run)
    //*********************************************************************
    {
        return slopeLength - run;
    }

    /**
     * @param allowance
     * @param gain
     * @return Allowance - Gain
     */
    //*********************************************************************
    private double getLengthBC(double allowance, double gain)
    //*********************************************************************
    {
        return allowance - gain;
    }

    /**
     * @param slopeLength
     * @param gain
     * @return Slope Length – Gain
     */
    //*********************************************************************
    private double getLengthAB(double slopeLength, double gain)
    //*********************************************************************
    {
        return slopeLength - gain;
    }

    /**
     * @param refLength
     * @param lengthBC
     * @param gain
     * @param run
     * @return Ref Length – ((length BC + Gain)+ Run)
     */
    //*********************************************************************
    private double getLengthA(double refLength, double lengthBC, double gain, double run)
    //*********************************************************************
    {
        return refLength - ((lengthBC + gain) + run);
    }

    /**
     * @param lengthA
     * @param lengthAB
     * @return Length A + length AB
     */
    //*********************************************************************
    private double getLengthB(double lengthA, double lengthAB)
    //*********************************************************************
    {
        return lengthA + lengthAB;
    }

    /**
     * @param lengthA
     * @param lengthAB
     * @param lengthBC
     * @return lengthA+lengthAB+lengthBC
     */
    //*********************************************************************
    private double getLengthC(double lengthA, double lengthAB, double lengthBC)
    //*********************************************************************
    {
        return lengthA + lengthAB + lengthBC;
    }
    //*********************************************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    //*********************************************************************
    {
        // Inflate the menu; this adds items to the action bar if it is pre
        getMenuInflater().inflate(R.menu.menu_info, menu);
        return true;
    }
    // ******************************************************************
    @Override
    protected void InfoUrl()
    // ******************************************************************
    {
        val url = getTutorialPath(HandTuberApplication.instance()
                                                      .getBendInfoByName(AndroidUtil.getString(
                                                              R.string.joint_offset_screen_name))
                                                      .getTutorialPath());
        Intent infoTutorialIntent = new Intent(this, InfoTutorialActivity.class);
        infoTutorialIntent.putExtra(InfoTutorialActivity.INFO_TITLE,
                                    AndroidUtil.getString(R.string.joint_offset_screen_name));
        infoTutorialIntent.putExtra(InfoTutorialActivity.SCREEN_URL, url);
        startActivity(infoTutorialIntent);
    }

}
