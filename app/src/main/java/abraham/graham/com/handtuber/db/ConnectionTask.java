package abraham.graham.com.handtuber.db;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Objects;

import abraham.graham.com.handtuber.R;
import abraham.graham.com.handtuber.Util.AndroidUtil;
import abraham.graham.com.handtuber.Util.DBCallBack;
import abraham.graham.com.handtuber.Util.UIResult;
import abraham.graham.com.handtuber.models.BendersModel;
import abraham.graham.com.handtuber.models.Bends;

//*********************************************************************
public class ConnectionTask extends AsyncTask<Object, Object, Object>
//*********************************************************************
{
    public static final int OT_INSERT_BENDS = 1;
    public static final int OT_FETCH_BENDS = OT_INSERT_BENDS + 1;
    public static final int OT_READ_BENDERS_FILE = OT_FETCH_BENDS + 1;
    public static final int OT_FETCH_BENDERS = OT_READ_BENDERS_FILE+1;
    public static final int OT_SAVE_BENDS=OT_FETCH_BENDERS+1;
    public static final int OT_UPDATE_BENDS=OT_SAVE_BENDS+1;
    private Context mContext;
    private int mOPType;
    private String mProgressMessage;
    private ProgressDialog mProgressDialog;
    boolean isProgress;
    public static final String TAG="ConnectionTask";
    //*********************************************************************
    public ConnectionTask(Context context, int type)
    //*********************************************************************
    {
        mContext = context;
        mOPType = type;
        Log.i(TAG,"ConnectionTask => "+type);
    }

    //*********************************************************************
    public ConnectionTask(Context context, int type, boolean progress,
                          String message)
    //*********************************************************************
    {
        mContext = context;
        mOPType = type;
        isProgress = progress;
        mProgressMessage = message;

    }

    //*********************************************************************
    @Override
    protected void onPreExecute()
    //*********************************************************************
    {
        if (isProgress)
        {
            mProgressDialog = ProgressDialog.show(mContext, AndroidUtil.getString(R.string.app_name), mProgressMessage, true);
        }

    }

    //*********************************************************************
    @Override
    protected void onPostExecute(Object object)
    //*********************************************************************
    {
        super.onPostExecute(object);
        if (isProgress && mProgressDialog.isShowing())
        {
            mProgressDialog.dismiss();
        }
        if (object != null)
        {
            ((UIResult) mContext).updateResult(object);

        }
        else
        {
            Toast.makeText(mContext, AndroidUtil.getString(R.string.error_op), Toast.LENGTH_SHORT)
                 .show();
        }
    }

    //*********************************************************************
    @Override
    protected Object doInBackground(Object... objects)
    //*********************************************************************
    {
        Object a = null;
        try
        {
            a = operationFactory(objects[0]);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return a;
    }

    //*********************************************************************
    private Object operationFactory(Object respectiveObject)
    //*********************************************************************
    {
        if (mOPType == OT_INSERT_BENDS && respectiveObject instanceof BendersModel)
        {
            return insertBends((BendersModel) respectiveObject);
        }
        else if (mOPType == OT_FETCH_BENDS)
        {
            return fetchBenders();
        }
        else if (mOPType == OT_READ_BENDERS_FILE)
        {
            return readJsonFile(mContext);
        }
        else if (mOPType == OT_FETCH_BENDERS)
        {
         return fetchBends();
        }
        else if (mOPType == OT_SAVE_BENDS)
        {
            return saveBends(mContext,respectiveObject);
        }
        else if (mOPType == OT_UPDATE_BENDS)
        {
            return updateBends(mContext,respectiveObject);
        }
        else
        {
            return null;
        }
    }
    //*********************************************************************
    private boolean updateBends(Context context, Object respectiveObject)
    //*********************************************************************
    {
        return PersistanceManager.getInstanceOf().updateBend((abraham.graham.com.handtuber.models.Bends)respectiveObject,mContext);
    }
    //*********************************************************************
    private boolean saveBends(Context context,Object respectiveObject)
    //*********************************************************************
    {
        return PersistanceManager.getInstanceOf().insertSingleBend((abraham.graham.com.handtuber.models.Bends)respectiveObject,mContext);
    }

    //*********************************************************************
    private boolean readJsonFile(Context context)
    //*********************************************************************
    {
        Log.i(TAG,"readJsonFile");
        boolean flag = false;
        try
        {
            JSONObject obj = new JSONObject(loadJSONFromAsset(context));
            JSONArray m_jArry = obj.getJSONArray("bender");
            if (m_jArry == null || m_jArry.length() == 0)
            {
                return flag;
            }
            ArrayList<abraham.graham.com.handtuber.models.Bends> bendsArray = new ArrayList<>(m_jArry.length());
            for (int i = 0; i < m_jArry.length(); i++)
            {
                abraham.graham.com.handtuber.models.Bends bendsObject = new abraham.graham.com.handtuber.models.Bends(m_jArry.getJSONObject(i), abraham.graham.com.handtuber.models.Bends.BEND_APP_SPECIFIC);
                if (bendsObject != null)
                    bendsArray.add(bendsObject);
            }
            // make database storage here
            Log.d("task","total count = > "+bendsArray.size());
            flag = bendsArray.size() > 0 && PersistanceManager.getInstanceOf().insertBends(bendsArray, context);
            if(flag)
            {
                SharedPreferences mPrefs = mContext.getSharedPreferences(AndroidUtil.getString(R.string.shared_preferences),
                                              Context.MODE_PRIVATE);
                mPrefs.edit()
                      .putInt(AndroidUtil.getString(R.string.default_unit_label), AndroidUtil.getResources()
                                                                                             .getInteger(
                                                                                                     R.integer.metric))
                      .commit();
                mPrefs.edit()
                      .putBoolean(AndroidUtil.getString(R.string.first_run), false)
                      .commit();
            }
        }
        catch (JSONException ex)
        {
            ex.printStackTrace();
            flag = false;
        }
        return flag;
    }

    //*********************************************************************
    public String loadJSONFromAsset(Context context)
    //*********************************************************************
    {
        String json = null;
        try
        {
            InputStream is = context.getAssets().open("benders.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    //*********************************************************************
    private ArrayList<BendersModel> fetchBenders()
    //*********************************************************************
    {
        PersistanceManager manager = PersistanceManager.getInstanceOf();
        return manager.fetchBenders(mContext);
    }

    //*********************************************************************
    private boolean insertBends(BendersModel model)
    //*********************************************************************
    {
        boolean flag = false;
        PersistanceManager persistanceManager = PersistanceManager.getInstanceOf();
        return persistanceManager.insertBenders(model, mContext);
    }

    //*********************************************************************
    private ArrayList<Bends>fetchBends()
    //*********************************************************************
    {
        return PersistanceManager.getInstanceOf().fetchBends(mContext);
    }
}
