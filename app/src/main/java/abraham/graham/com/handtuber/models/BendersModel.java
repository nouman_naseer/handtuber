package abraham.graham.com.handtuber.models;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Iterator;

import abraham.graham.com.handtuber.db.DBConstants;
import lombok.Getter;
import lombok.Setter;

//*********************************************************************
public class BendersModel implements Parcelable
//*********************************************************************
{
    private @Getter @Setter String inputValues;
    private @Getter @Setter String outputValues;
    private @Getter @Setter String benderName;
    private @Getter @Setter int bendType;
    private @Getter @Setter String bendId;
    private @Getter @Setter BendersModel model;
    private @Getter @Setter HashMap<String,String> inputMap;
    private @Getter @Setter HashMap<String,String>outputMap;

    //*********************************************************************
    public void saveModel()
    //*********************************************************************
    {
        //save the input & output values as Json String. 
        if(!inputMap.isEmpty())
        {
            JSONArray inputValuesArray = new JSONArray();
            for(String key: inputMap.keySet())
            {
                JSONObject singleValue = new JSONObject();
                try
                {
                    singleValue.put(key,inputMap.get(key));
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
                inputValuesArray.put(singleValue);
            }
            this.setInputValues(inputValuesArray.toString());
        }

        if(!outputMap.isEmpty())
        {
            JSONArray outputValuesArray = new JSONArray();
            for(String key: outputMap.keySet())
            {
                JSONObject singleValue = new JSONObject();
                try
                {
                    singleValue.put(key,outputMap.get(key));
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
                outputValuesArray.put(singleValue);
            }

            this.setOutputValues(outputValuesArray.toString());
        }

    }

    //*********************************************************************
    public void setIOValues()
    //*********************************************************************
    {
        if (TextUtils.isEmpty(getInputValues()) && TextUtils.isEmpty(getOutputValues()))
            return;
        setInputMap(readMap(getInputValues()));
        setOutputMap(readMap(getOutputValues()));
    }

    //*********************************************************************
    private HashMap<String,String>readMap(String ioValue)
    //*********************************************************************
    {
        HashMap<String, String> localMap = null;
        try
        {
            JSONArray jsonArray = new JSONArray(ioValue);
            localMap = new HashMap<>();
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject object = new JSONObject(jsonArray.get(i).toString());
                Iterator<String> keyset = object.keys();
                while (keyset.hasNext())
                {
                    String key = (String) keyset.next();
                    String value = object.getString(key);
                    localMap.put(key, value);
                }
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            localMap = null;
        }
        return localMap;
    }

    //*********************************************************************
    public static BendersModel getModel(Cursor cursor)
    //*********************************************************************
    {
        BendersModel selectedModel = new BendersModel();
        selectedModel.setBenderName(cursor.getString(cursor.getColumnIndex(DBConstants.BENDERS_COL_NAME)));
        selectedModel.setBendType(cursor.getInt(cursor.getColumnIndex(DBConstants.BENDERS_COL_TYPE)));
        selectedModel.setInputValues(cursor.getString(cursor.getColumnIndex(DBConstants.BENDERS_COL_INPUT_VALUE)));
        selectedModel.setOutputValues(cursor.getString(cursor.getColumnIndex(DBConstants.BENDERS_COL_OUTPUT_VALUE)));
        return selectedModel;
    }

    //*********************************************************************
    @Override
    public String toString()
    //*********************************************************************
    {
        return "BendersModel{" +
                "inputValues='" + inputValues + '\'' +
                ", outputValues='" + outputValues + '\'' +
                ", benderName='" + benderName + '\'' +
                ", bendType=" + bendType +
                ", bendId='" + bendId + '\'' +
                ", model=" + model +
                ", inputMap=" + inputMap +
                ", outputMap=" + outputMap +
                '}';
    }

    //*********************************************************************
    public  BendersModel()
    //*********************************************************************
    {

    }


    //*********************************************************************
    public static final Parcelable.Creator<BendersModel> CREATOR = new Creator<BendersModel>()
    //*********************************************************************
    {

        //*********************************************************************
        @Override
        public BendersModel[] newArray(int size)
        //*********************************************************************
        {
            return new BendersModel[size];
        }

        //*********************************************************************
        @Override
        public BendersModel createFromParcel(Parcel source)
        //*********************************************************************
        {
            return new BendersModel(source);
        }
    };

    //*********************************************************************
    @Override
    public int describeContents()
    //*********************************************************************
    {
        return 0;
    }

    //*********************************************************************
    public BendersModel(Parcel in )
    //*********************************************************************
    {
        readFromParcel(in);
    }

    //*********************************************************************
    @Override
    public void writeToParcel(Parcel dest, int i)
    //*********************************************************************
    {
        dest.writeInt(getBendType());
        dest.writeString(getBenderName());
        dest.writeString(getInputValues());
        dest.writeString(getOutputValues());

    }

    //*********************************************************************
    private void readFromParcel(Parcel in )
    //*********************************************************************
    {
        setBendType(in.readInt());
        setBenderName(in.readString());
        setInputValues(in.readString());
        setOutputValues(in.readString());
    }
}
