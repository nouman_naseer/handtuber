package abraham.graham.com.handtuber.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import abraham.graham.com.handtuber.HandTuberApplication;
import abraham.graham.com.handtuber.R;
import abraham.graham.com.handtuber.activities.BackbendOffsetActivity;
import abraham.graham.com.handtuber.activities.FallCalculator;
import abraham.graham.com.handtuber.activities.JointOffsetActivity;
import abraham.graham.com.handtuber.activities.LongRunOffsetActivity;
import abraham.graham.com.handtuber.activities.ParallelOffsetActivity;
import abraham.graham.com.handtuber.activities.PredictiveOffsetActivity;
import abraham.graham.com.handtuber.activities.SimpleOffsetActivity;
import abraham.graham.com.handtuber.adapters.BendAdapter;
import abraham.graham.com.handtuber.models.BendsModel;
import lombok.val;


//*********************************************************************
public class BendsFragment extends BaseFragment
//*********************************************************************
{
    private ListView mBendsListView;
    private ArrayList<BendsModel> mBendList;
    private final String TAG = "bend-fragment";

    //*********************************************************************
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    //*********************************************************************
    {
        super.onCreate(savedInstanceState);
    }

    //*********************************************************************
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    //*********************************************************************
    {
        val view = inflater.inflate(R.layout.bends_fragment_layout, container, false);
        mBendsListView = (ListView) view.findViewById(R.id.bend_list);
        mBendList = HandTuberApplication.instance().getBendInfoList();
        mBendsListView.setAdapter(new BendAdapter(getActivity(),mBendList));
        mBendsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                moveToScreen(position);
            }
        });
        return view;
    }


    //*********************************************************************
    private void moveToScreen(int position)
    //*********************************************************************
    {
        Intent intent=null;
        switch (position)
        {
            case 0:
                intent = new Intent(getActivity(), SimpleOffsetActivity.class);
                break;
            case 1:
                intent = new Intent(getActivity(), PredictiveOffsetActivity.class);
                break;
            case 2:
                intent = new Intent(getActivity(), ParallelOffsetActivity.class);
                break;
            case 3:
                intent = new Intent(getActivity(), LongRunOffsetActivity.class);
                break;
            case 4:
                intent = new Intent(getActivity(), JointOffsetActivity.class);
                break;
            case 5:
                intent = new Intent(getActivity(), FallCalculator.class);
                break;
            case 6:
                intent = new Intent(getActivity(), BackbendOffsetActivity.class);
                break;
        }

        if(intent==null)
            return;
        startActivity(intent);
    }


}
