package abraham.graham.com.handtuber.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import abraham.graham.com.handtuber.R;
import abraham.graham.com.handtuber.Util.AndroidUtil;
import abraham.graham.com.handtuber.activities.SimpleOffsetActivity;
import abraham.graham.com.handtuber.models.BendsModel;
import lombok.Setter;


//*********************************************************************
public class BendAdapter extends BaseAdapter
//*********************************************************************
{

    private ArrayList<BendsModel> mBendList;
    private LayoutInflater mLayoutInflater;

    //*********************************************************************
    public BendAdapter(Activity activity, ArrayList<BendsModel> bendList)
    //*********************************************************************
    {
        super();
        mLayoutInflater = LayoutInflater.from(activity);
        mBendList = bendList;
    }

    //*********************************************************************
    public void bindView(View view, int position)
    //*********************************************************************
    {
        BendViewHolder bendHolder = getViewHolder(view);
        bendHolder.bendName.setText(mBendList.get(position).getBendName());
        bendHolder.bendImage.setImageDrawable(AndroidUtil.getDrawable(mBendList.get(position).getBendResourceId()));
    }

    //*********************************************************************
    @Override
    public int getCount()
    //*********************************************************************
    {
        return mBendList.size();
    }

    //*********************************************************************
    @Override
    public Object getItem(int i)
    //*********************************************************************
    {
        return mBendList.get(i);
    }

    //*********************************************************************
    @Override
    public long getItemId(int i)
    //*********************************************************************
    {
        return 0;
    }

    // ******************************************************************
    private View newView(ViewGroup parent, BendsModel item)
    // ******************************************************************
    {
        return mLayoutInflater.inflate(R.layout.bends_row_layout, parent, false);
    }

    //*********************************************************************
    @Override
    public View getView(int i, View view, ViewGroup parent)
    //*********************************************************************
    {
        final BendsModel item = mBendList.get(i);
        if (view == null)
            view = newView(parent, item);
        bindView(view, i);
        return view;
    }

    //*********************************************************************
    private static final class BendViewHolder
    //*********************************************************************
    {
        public ImageView bendImage;
        public TextView bendName;
        public LinearLayout parent;
    }

    //*********************************************************************
    private static BendViewHolder getViewHolder(View itemView)
    //*********************************************************************
    {
        BendViewHolder holder = (BendViewHolder) itemView.getTag();
        if (holder == null)
        {
            holder = new BendViewHolder();
            holder.bendImage = (ImageView) itemView.findViewById(R.id.bend_image);
            holder.bendName = (TextView) itemView.findViewById(R.id.bend_name);
            holder.parent = (LinearLayout) itemView.findViewById(R.id.parent);
            itemView.setTag(holder);
        }
        return holder;
    }


}
