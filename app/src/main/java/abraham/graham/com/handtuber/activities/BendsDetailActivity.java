package abraham.graham.com.handtuber.activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import abraham.graham.com.handtuber.R;
import abraham.graham.com.handtuber.Util.AndroidUtil;
import abraham.graham.com.handtuber.Util.RealPathUtil;
import abraham.graham.com.handtuber.Util.UIResult;
import abraham.graham.com.handtuber.db.ConnectionTask;
import abraham.graham.com.handtuber.models.Bends;
import abraham.graham.com.handtuber.models.MessageEvent;
import lombok.val;

/**
 * Created by nouman on 2/16/17.
 */

//*********************************************************************
public class BendsDetailActivity
        extends BaseActivity
        implements View.OnClickListener, UIResult
//*********************************************************************
{

    private EditText mTypeEditText;
    private TextView mMetricTextView;
    private EditText mSizeEditText;
    private EditText mRadiusEditText;
    private ImageView mSelectedImage;
    private TextInputLayout mTypeInputLayout;
    private TextInputLayout mSizeInputLayout;
    private TextInputLayout mRadiusInputLayout;
    private int defaultUnit;
    private String[] mSelectedUnits = AndroidUtil.getStringArray(R.array.units_array);
    private int selectedUnit;
    private String[] mOptions = new String[] { "Camera", "Gallery" };
    private int mSelectedChoice;
    private final static int CHOICE_CAMERA = 0;
    private final static int CHOICE_GALLERY = CHOICE_CAMERA + 1;
    private Uri mImageUri;
    public static final String BENDERS_DISPLAY = "display_benders";
    private SwitchCompat mDefaultSwitch;
    private boolean isUpdateRecord = false;
    private Bends selectedBender;

    //*********************************************************************
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    //*********************************************************************
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.benders_detail_layout);
        val toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initControls();
    }

    //*********************************************************************
    private void initControls()
    //*********************************************************************
    {
        mTypeEditText = (EditText) findViewById(R.id.txt_type);
        mMetricTextView = (TextView) findViewById(R.id.label_unit_selection);
        mSizeEditText = (EditText) findViewById(R.id.txt_size);
        mRadiusEditText = (EditText) findViewById(R.id.txt_radius);
        mTypeInputLayout = (TextInputLayout) findViewById(R.id.txt_type_wrapper);
        mRadiusInputLayout = (TextInputLayout) findViewById(R.id.txt_radius_wrapper);
        mSizeInputLayout = (TextInputLayout) findViewById(R.id.txt_size_wrapper);
        mMetricTextView.setOnClickListener(this);
        mSelectedImage = (ImageView) findViewById(R.id.image);
        mSelectedImage.setOnClickListener(this);
        mDefaultSwitch = (SwitchCompat) findViewById(R.id.switch_default);
        if (ContextCompat.checkSelfPermission(this,
                                              Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this,
                                              new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE },
                                              0);
        }

        if (ContextCompat.checkSelfPermission(this,
                                              Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this,
                                              new String[] { Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE },
                                              0);
        }
        populateControls();

    }


    //*********************************************************************
    private void hideKeyboard()
    //*********************************************************************
    {
        val view = getCurrentFocus();
        if (view != null)
        {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
                                                                                         hideSoftInputFromWindow(
                                                                                                 view.getWindowToken(),
                                                                                                 InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    //*********************************************************************
    private void saveBender()
    //*********************************************************************
    {
        hideKeyboard();

        if (TextUtils.isEmpty(mTypeEditText.getText()))
        {
            mTypeInputLayout.setError(AndroidUtil.getString(R.string.message_empty_type));
            return;
        }
        if (TextUtils.isEmpty(mSizeEditText.getText()))
        {
            mSizeInputLayout.setError(AndroidUtil.getString(R.string.message_empty_type));
            return;
        }

        if (TextUtils.isEmpty(mRadiusEditText.getText()))
        {
            mRadiusInputLayout.setError(AndroidUtil.getString(R.string.message_empty_type));
            return;
        }

        disableError(mTypeInputLayout);
        disableError(mSizeInputLayout);
        disableError(mRadiusInputLayout);

        if (!isUpdateRecord) //create new record
        {
            Bends selectedModel = new Bends();
            selectedModel.setMeasuringUnit(selectedUnit);
            selectedModel.setName(mTypeEditText.getText()
                                               .toString());
            selectedModel.setRadius(mRadiusEditText.getText()
                                                   .toString());
            selectedModel.setDefaultBender(mDefaultSwitch.isChecked() ? 1 : 0);
            selectedModel.setBendType(Bends.BEND_USER_SPECIFIC);
            selectedModel.setSize(mSizeEditText.getText()
                                               .toString());
            selectedModel.setType(Bends.BEND_TYPE_USER_SPECIFIC);
            selectedModel.setFilePath(filePath);
            ConnectionTask task = new ConnectionTask(this, ConnectionTask.OT_SAVE_BENDS);
            task.execute(selectedModel);
        }
        else
        {
            selectedBender.setMeasuringUnit(selectedUnit);
            selectedBender.setName(mTypeEditText.getText()
                                                .toString());
            selectedBender.setRadius(mRadiusEditText.getText()
                                                    .toString());
            selectedBender.setDefaultBender(mDefaultSwitch.isChecked() ? 1 : 0);

            selectedBender.setSize(mSizeEditText.getText()
                                                .toString());
            ConnectionTask task = new ConnectionTask(this, ConnectionTask.OT_UPDATE_BENDS);
            task.execute(selectedBender);
        }


    }

    //*********************************************************************
    private void populateControls()
    //*********************************************************************
    {
        if (getIntent().getExtras() == null || !getIntent().getExtras()
                                                           .containsKey(BENDERS_DISPLAY))
            return;
        selectedBender = getIntent().getExtras()
                                    .getParcelable(BENDERS_DISPLAY);

        if (selectedBender == null)
            return;

        mTypeEditText.setText(selectedBender.getName());
        mRadiusEditText.setText(selectedBender.getRadius());
        mDefaultSwitch.setChecked(selectedBender.getDefaultBender() == 1);
        mSizeEditText.setText(selectedBender.getSize());
        filePath = selectedBender.getFilePath();
        selectedUnit = selectedBender.getMeasuringUnit();
        updateUnitControls();

        if (selectedBender.getBendType() == Bends.BEND_APP_SPECIFIC)
        {
            //load the image from assets.
            Picasso.with(this).load(AndroidUtil.getAssetImagePath(selectedBender.getFilePath()))
                   .placeholder(AndroidUtil.getDrawable(R.mipmap.icon_bender_placeholder))
                   .into(mSelectedImage);
        }
        else
        {
            //check if the file path empty, use default image.
            if (TextUtils.isEmpty(selectedBender.getFilePath()))
                Picasso.with(this)
                       .load(R.mipmap.icon_bender_placeholder)
                       .into(mSelectedImage);
            else
            {
                File file = new File(selectedBender.getFilePath());
                Picasso.with(this)
                       .load(file)
                       .error(R.mipmap.icon_bender_placeholder)
                       .into(mSelectedImage);
            }

        }
        isUpdateRecord = true;
    }


    /*
    Disable error from the input layout
     */
    //*********************************************************************
    private void disableError(TextInputLayout layout)
    //*********************************************************************
    {
        layout.setErrorEnabled(false);
    }

    //*********************************************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    //*********************************************************************
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;
    }

    //*********************************************************************

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    //*********************************************************************
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_save)
        {
            saveBender();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // ******************************************************************
    @Override
    public void updateResult(Object result)
    // ******************************************************************
    {
        if (result instanceof Boolean)
        {
            boolean flag = (boolean) result;
            if (flag)
            {
                Toast.makeText(BendsDetailActivity.this,
                               AndroidUtil.getString(R.string.bends_saved), Toast.LENGTH_SHORT)
                     .show();
                MessageEvent event = new MessageEvent(MessageEvent.UPDATE_BENDERS_TABLE);
                EventBus.getDefault().post(event);
            }
            else
            {
                Toast.makeText(BendsDetailActivity.this,
                               AndroidUtil.getString(R.string.bends_error), Toast.LENGTH_SHORT)
                     .show();
            }
            finish();
        }
    }


    // ******************************************************************
    private void displayUnitSelectionDialog()
    // ******************************************************************
    {
        //display dialog of the list of units + selected unit
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(BendsDetailActivity.this);
        // ******************************************************************
        alertDialog.setTitle(AndroidUtil.getString(R.string.selection_unit_dialog_title))
                   .setSingleChoiceItems(mSelectedUnits, defaultUnit,
                                         new DialogInterface.OnClickListener()
                                                 // ******************************************************************
                                         {
                                             @Override
                                             public void onClick(DialogInterface dialogInterface, int i)
                                             {
                                                 if (i != defaultUnit)
                                                     selectedUnit = i;
                                                 updateUnitControls();
                                             }

                                         })
                   .setPositiveButton(AndroidUtil.getString(R.string.selection_ok),
                                      new DialogInterface.OnClickListener()

                                      {
                                          @Override
                                          public void onClick(DialogInterface dialogInterface, int i)
                                          {
                                              dialogInterface.dismiss();
                                          }
                                      });

        final AlertDialog dialog = alertDialog.create();
        dialog.show();
    }


    // ******************************************************************
    private void updateUnitControls()
    // ******************************************************************
    {
        defaultUnit = selectedUnit;
        if (defaultUnit <= mSelectedUnits.length)
            mMetricTextView.setText(mSelectedUnits[defaultUnit]);
    }


    //*********************************************************************
    private void takePicture()
    //*********************************************************************
    {
        //display dialog of the list of units + selected unit
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(BendsDetailActivity.this);
        alertDialog.setTitle(AndroidUtil.getString(R.string.picture_dialog_title))
                   .setAdapter(
                           new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item,
                                              mOptions),
                           new DialogInterface.OnClickListener()
                           {
                               @Override
                               public void onClick(DialogInterface dialogInterface, int i)
                               {
                                   mSelectedChoice = i;
                                   startCaptureByChoice();
                                   dialogInterface.dismiss();
                               }
                           });

        final AlertDialog dialog = alertDialog.create();
        dialog.show();

    }


    private void startCaptureByChoice()
    {
        switch (mSelectedChoice)
        {
        case CHOICE_CAMERA:
            startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                                           .putExtra(MediaStore.EXTRA_OUTPUT, getOutputMediaFileUri(
                                                   BendsDetailActivity.this)),
                                   CHOICE_CAMERA);
            break;
        case CHOICE_GALLERY:
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/*");
            startActivityForResult(intent, CHOICE_GALLERY);
            break;
        }
    }

    Uri selectedImageUri;

    //*********************************************************************
    public Uri getOutputMediaFileUri(Context context)
    //*********************************************************************
    {
        selectedImageUri = FileProvider.getUriForFile(context, context.getApplicationContext()
                                                                      .getPackageName() + ".provider",
                                                      getOutputMediaFile());
        return selectedImageUri;
    }


    //*********************************************************************
    private File getOutputMediaFile()
    //*********************************************************************
    {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), AndroidUtil.getString(R.string.app_name));

        if (!mediaStorageDir.exists())
        {
            if (!mediaStorageDir.mkdirs())
            {
                Log.d("CameraDemo", "failed to create directory");
                return null;
            }
        }


        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        filePath = mediaStorageDir.getPath() + File.separator +
                "IMG_" + timeStamp + ".jpg";
        return new File(filePath);
    }


    private String filePath;

    //*********************************************************************
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    //*********************************************************************
    {
        if (requestCode == 0)
        {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)
            {
                mSelectedImage.setEnabled(true);
            }
        }
    }

    //*********************************************************************
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    //*********************************************************************
    {
        switch (requestCode)
        {
        case CHOICE_CAMERA:
            if (resultCode == RESULT_OK)
            {
                if (selectedImageUri != null)
                {
                    mSelectedImage.setImageURI(selectedImageUri);
                }
            }
            break;
        case CHOICE_GALLERY:
            if (resultCode == RESULT_OK)
            {
                mImageUri = data.getData();
                if (mImageUri != null)
                {
                    filePath = RealPathUtil.getRealPathFromURI_API19(BendsDetailActivity.this,
                                                                     mImageUri);
                    Picasso.with(BendsDetailActivity.this)
                           .load(mImageUri)
                           .error(AndroidUtil.getDrawable(R.mipmap.icon_bender_placeholder))
                           .into(mSelectedImage);

                }
            }
            break;
        }
    }

    //*********************************************************************
    public String getRealPathFromURI(Context context, Uri contentUri)
    //*********************************************************************
    {

        Cursor cursor = null;

        try
        {

            String[] proj = { MediaStore.Images.Media.DATA };

            cursor = context.getContentResolver()
                            .query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        finally
        {
            if (cursor != null)
            {
                cursor.close();
            }
        }
    }

    //*********************************************************************
    @Override
    public void onClick(View view)
    //*********************************************************************
    {
        switch (view.getId())
        {
        case R.id.label_unit_selection:
            displayUnitSelectionDialog();
            break;
        case R.id.image:
            takePicture();
            break;
        default:
            break;
        }
    }


}
