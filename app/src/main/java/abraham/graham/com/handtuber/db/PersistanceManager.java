package abraham.graham.com.handtuber.db;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.lang.reflect.Array;
import java.util.ArrayList;

import abraham.graham.com.handtuber.Util.Bends;
import abraham.graham.com.handtuber.models.BendersModel;

//*********************************************************************
public class PersistanceManager
//*********************************************************************
{
    private static PersistanceManager mDbManager = null;

    //*********************************************************************
    public static PersistanceManager getInstanceOf()
    //*********************************************************************
    {
        if (mDbManager != null)
        {
            return mDbManager;
        }
        else
        {
            mDbManager = new PersistanceManager();
            return mDbManager;
        }
    }

    //*********************************************************************
    private PersistanceManager()
    //*********************************************************************
    {

    }

    //*********************************************************************
    public boolean insertSingleBend(abraham.graham.com.handtuber.models.Bends model, Context context)
    //*********************************************************************
    {
        boolean flag = false;

        DatabaseAdapter databaseAdapter = null;
        try
        {
            databaseAdapter = new DatabaseAdapter(context).open();
            long insertedRow = databaseAdapter.insertBend(model);
            databaseAdapter.close();
            flag = insertedRow != -1 ? true : false;

        }
        catch (Exception e)
        {
            Log.d("DB_ROUTE", e.getMessage());
            flag = false;
        }
        return flag;
    }

    //*********************************************************************
    public boolean insertBenders(BendersModel model, Context context)
    //*********************************************************************
    {
        boolean flag = false;

        DatabaseAdapter databaseAdapter = null;
        try
        {
            databaseAdapter = new DatabaseAdapter(context).open();
            long insertedRow = databaseAdapter.insertBender(model);
            databaseAdapter.close();
            flag = insertedRow != -1 ? true : false;

        }
        catch (Exception e)
        {
            Log.d("DB_ROUTE", e.getMessage());
            flag = false;
        }
        return flag;
    }

    //*********************************************************************
    public boolean insertBends(ArrayList<abraham.graham.com.handtuber.models.Bends> modelList, Context context)
    //*********************************************************************
    {
        boolean flag = false;

        DatabaseAdapter databaseAdapter = null;
        try
        {
            for (abraham.graham.com.handtuber.models.Bends model : modelList)
            {
                databaseAdapter = new DatabaseAdapter(context).open();
                long insertedRow = databaseAdapter.insertBend(model);
                databaseAdapter.close();
                flag = insertedRow != -1 ? true : false;
            }

        }
        catch (Exception e)
        {
            Log.d("DB_ROUTE", e.getMessage());
            flag = false;
        }
        return flag;
    }


    //*********************************************************************
    public ArrayList<BendersModel> fetchBenders(Context context)
    //*********************************************************************
    {
        DatabaseAdapter adapter = new DatabaseAdapter(context).open();
        Cursor bendCursor = null;
        BendersModel model = null;
        ArrayList<BendersModel> bendersList = new ArrayList<>();
        try
        {
            bendCursor = adapter.fetchBenders();
            bendCursor.moveToFirst();
            while (!bendCursor.isAfterLast())
            {
                model = new BendersModel();
                model.setBenderName(bendCursor.getString(
                        bendCursor.getColumnIndex(DBConstants.BENDERS_COL_NAME)));
                model.setBendType(
                        bendCursor.getInt(bendCursor.getColumnIndex(DBConstants.BENDERS_COL_NAME)));
                model.setInputValues(bendCursor.getString(
                        bendCursor.getColumnIndex(DBConstants.BENDERS_COL_INPUT_VALUE)));
                model.setOutputValues(bendCursor.getString(
                        bendCursor.getColumnIndex(DBConstants.BENDERS_COL_OUTPUT_VALUE)));
                bendersList.add(model);
                bendCursor.move(1);
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            bendersList = null;
        }

        return bendersList;
    }


    //*********************************************************************
    public ArrayList<abraham.graham.com.handtuber.models.Bends> fetchBends(Context context)
    //*********************************************************************
    {
        DatabaseAdapter adapter = new DatabaseAdapter(context).open();
        Cursor bendCursor = null;
        abraham.graham.com.handtuber.models.Bends model = null;
        ArrayList<abraham.graham.com.handtuber.models.Bends> bendersList = new ArrayList<>();
        try
        {
            bendCursor = adapter.fetchBends();
            Log.d("DB", "CursorCount => " + bendCursor.getCount());
            bendCursor.moveToFirst();
            while (!bendCursor.isAfterLast())
            {
                model = new abraham.graham.com.handtuber.models.Bends();
                model.set_id(bendCursor.getInt(bendCursor.getColumnIndex("_id")));
                model.setName(bendCursor.getString(
                        bendCursor.getColumnIndex(DBConstants.BENDS_COL_NAME)));
                model.setBendType(
                        bendCursor.getInt(bendCursor.getColumnIndex(DBConstants.BENDS_COL_TYPE)));
                model.setRadius(bendCursor.getString(
                        bendCursor.getColumnIndex(DBConstants.BENDS_COL_RADIUS)));
                model.setSize(bendCursor.getString(
                        bendCursor.getColumnIndex(DBConstants.BENDS_COL_SIZE)));
                model.setMeasureUnit(bendCursor.getString(
                        bendCursor.getColumnIndex(DBConstants.BENDS_COL_UNIT)));
                model.setFilePath(bendCursor.getString(
                        bendCursor.getColumnIndex(DBConstants.BENDS_COL_PATH)));
                model.setDefaultBender(bendCursor.getInt(
                        bendCursor.getColumnIndex(DBConstants.BENDS_COL_DEFAULT)));
                bendersList.add(model);
                bendCursor.move(1);
            }


        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            bendersList = null;
        }

        finally
        {
            adapter.close();
        }

        return bendersList;
    }

    /*
    update Bend
    -- check if setDefault == true
    -- check for defaultBend
    -- compare ids of both bends
    -- if IDs matches update the bend
    -- if Ids don't match make already default bend false, and this bend as true. By updating both bends.
    -- check if setDefault == false;
    -- update the bend simply.
     */
    //*********************************************************************
    public boolean updateBend(abraham.graham.com.handtuber.models.Bends selectedBend, Context context)
    //*********************************************************************
    {
        boolean flag = false;
        if (selectedBend.getDefaultBender() == abraham.graham.com.handtuber.models.Bends.BEND_DEFAULT)
        {
            abraham.graham.com.handtuber.models.Bends defaultBend = getDefaultBend(context);
            if (defaultBend.get_id() == selectedBend.get_id())
            {
                flag = updateBends(selectedBend, context);
            }
            else
            {
                //make defaultbend = nondefault and update
                defaultBend.setDefaultBender(
                        abraham.graham.com.handtuber.models.Bends.BEND_NON_DEFAULT);
                flag = updateBends(defaultBend, context);
                flag = updateBends(selectedBend, context);
            }
        }
        else
        {
                flag = updateBends(selectedBend, context);
        }
        return flag;
    }

    //*********************************************************************
    public static abraham.graham.com.handtuber.models.Bends getDefaultBend(Context context)
    //*********************************************************************
    {
        boolean flag = false;
        DatabaseAdapter adapter = new DatabaseAdapter(context).open();
        Cursor bendCursor = null;
        ArrayList<abraham.graham.com.handtuber.models.Bends> bendersList = new ArrayList<>();
        abraham.graham.com.handtuber.models.Bends model;
        try
        {
            bendCursor = adapter.fetchDefaultBend();
            Log.d("DB", "CursorCount => " + bendCursor.getCount());
            bendCursor.moveToFirst();
            while (!bendCursor.isAfterLast())
            {
                model = new abraham.graham.com.handtuber.models.Bends();
                model.set_id(bendCursor.getInt(bendCursor.getColumnIndex("_id")));
                model.setName(bendCursor.getString(
                        bendCursor.getColumnIndex(DBConstants.BENDS_COL_NAME)));
                model.setBendType(
                        bendCursor.getInt(bendCursor.getColumnIndex(DBConstants.BENDS_COL_TYPE)));
                model.setRadius(bendCursor.getString(
                        bendCursor.getColumnIndex(DBConstants.BENDS_COL_RADIUS)));
                model.setSize(bendCursor.getString(
                        bendCursor.getColumnIndex(DBConstants.BENDS_COL_SIZE)));
                model.setMeasureUnit(bendCursor.getString(
                        bendCursor.getColumnIndex(DBConstants.BENDS_COL_UNIT)));
                model.setFilePath(bendCursor.getString(
                        bendCursor.getColumnIndex(DBConstants.BENDS_COL_PATH)));
                model.setDefaultBender(bendCursor.getInt(
                        bendCursor.getColumnIndex(DBConstants.BENDS_COL_DEFAULT)));
                bendersList.add(model);
                bendCursor.move(1);
            }


        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            bendersList = null;
        }
        finally
        {
            adapter.close();
        }

        return bendersList.get(0);
    }

    //*********************************************************************
    public static boolean updateBends(abraham.graham.com.handtuber.models.Bends model, Context context)
    //*********************************************************************
    {
        boolean flag = false;

        DatabaseAdapter databaseAdapter = null;
        try
        {
            databaseAdapter = new DatabaseAdapter(context).open();
            long insertedRow = databaseAdapter.updateBends(model);
            databaseAdapter.close();
            flag = insertedRow != -1 ? true : false;
        }
        catch (Exception e)
        {
            Log.d("DB_ROUTE", e.getMessage());
            flag = false;
        }
        return flag;
    }

    //*********************************************************************
    public Cursor fetchBenderCursor(Context context)
    //*********************************************************************
    {
        DatabaseAdapter adapter = new DatabaseAdapter(context).open();
        Cursor bendCursor = null;
        try
        {
            bendCursor = adapter.fetchBenders();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            bendCursor = null;
        }
        return bendCursor;
    }
}
