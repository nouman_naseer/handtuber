package abraham.graham.com.handtuber.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.HashMap;

import abraham.graham.com.handtuber.HandTuberApplication;
import abraham.graham.com.handtuber.R;
import abraham.graham.com.handtuber.Util.AndroidUtil;
import abraham.graham.com.handtuber.Util.Bends;
import abraham.graham.com.handtuber.Util.Utils;
import abraham.graham.com.handtuber.db.ConnectionTask;
import abraham.graham.com.handtuber.models.BendersModel;
import lombok.val;

//*********************************************************************
public class PredictiveOffsetActivity extends BaseActivity
//*********************************************************************
{

    EditText mBendAngle;
    EditText mOffset;
    TextView mLabelAngle;
    TextView mLabelMeasure;
    TextView mLabelRun;
    TextView mLabelOffset;
    TextView mValueMeasure;
    TextView mValueRun;
    Button mCalculate;
    TextView mSelectedBender;

    public static final String TAG = "PREDRICTIVE_OFFSET";
    public static String INPUT_FIELDKEY_ANGLE = AndroidUtil.getString(R.string.label_angle);
    public static String INPUT_FIELDKEY_OFFSET=AndroidUtil.getString(R.string.label_offset);
    public static String OUTPUT_FIELD_MEASURE=AndroidUtil.getString(R.string.label_measure_to);
    public static String OUTPUT_FIELD_RUN=AndroidUtil.getString(R.string.label_run);

    //*********************************************************************
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    //*********************************************************************
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.predictive_offset_layout);
        val toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initControls();

    }

    // ******************************************************************
    private void initControls()
    // ******************************************************************
    {
        mBendAngle = (EditText) findViewById(R.id.input_angle);
        mOffset = (EditText) findViewById(R.id.input_offset);
        mCalculate = (Button) findViewById(R.id.btn_cal);

        mCalculate.setOnClickListener(new View.OnClickListener() {
            // ******************************************************************
            @Override
            public void onClick(View view)
            // ******************************************************************
            {
                computeValues();
            }
        });

        mValueMeasure = (TextView) findViewById(R.id.label_measurement_value);
        mValueRun = (TextView) findViewById(R.id.label_run_value);
        mLabelAngle = (TextView) findViewById(R.id.txt_diagram_angle);
        mLabelMeasure = (TextView) findViewById(R.id.txt_diagram_measure);
        mLabelRun = (TextView) findViewById(R.id.txt_diagram_run);
        mLabelOffset = (TextView) findViewById(R.id.txt_diagram_offset);
        mSelectedBender = (TextView) findViewById(R.id.label_bender_value);
        populateData();
    }

    // ******************************************************************
    private void computeValues()
    // ******************************************************************
    {
        if (TextUtils.isEmpty(mBendAngle.getText().toString()))
            return;
        if (TextUtils.isEmpty(mOffset.getText().toString()))
            return;


        //Utils.getBenderRadius();
        double bendAngle = Double.valueOf(mBendAngle.getText().toString());
        double offset = Double.valueOf(mOffset.getText().toString());
        mLabelAngle.setText(Double.toString(bendAngle));
        mLabelOffset.setText(Double.toString(offset));

        double calculatedGain = getCalculatedGain(bendAngle, BENDER_RADIUS);

        Log.d(TAG, "calculatedGain => " + calculatedGain);

        double calculatedOffset = getCalculatedOffset(offset, bendAngle);
        Log.d(TAG, "calculatedOffset => " + calculatedOffset);

        double measureTo = getMeasureTo(calculatedGain, calculatedOffset);

        Log.d(TAG, "measureTo => " + measureTo);

        mValueMeasure.setText(Double.toString(measureTo));
        mLabelMeasure.setText(Double.toString(measureTo));

        double run = getRun(offset, bendAngle);

        Log.d(TAG, "run => " + run);
        mValueRun.setText(Double.toString(run));
        mLabelRun.setText(Double.toString(run));
        isComputed = true;
    }

    // ******************************************************************
    private void populateData()
    // ******************************************************************
    {
        BendersModel model = getModel();
        if (model == null)
            return;
        model.setIOValues();
        mBendAngle.setText(model.getInputMap().get(INPUT_FIELDKEY_ANGLE));
        mLabelAngle.setText(model.getInputMap().get(INPUT_FIELDKEY_ANGLE));

        mOffset.setText(model.getInputMap().get(INPUT_FIELDKEY_OFFSET));
        mLabelOffset.setText(model.getInputMap().get(INPUT_FIELDKEY_OFFSET));

        mValueRun.setText(model.getOutputMap().get(OUTPUT_FIELD_RUN));
        mLabelRun.setText(model.getOutputMap().get(OUTPUT_FIELD_RUN));

        mValueMeasure.setText(model.getOutputMap().get(OUTPUT_FIELD_MEASURE));
        mLabelMeasure.setText(model.getOutputMap().get(OUTPUT_FIELD_MEASURE));

    }


    // ******************************************************************
    @Override
    protected void saveData()
    // ******************************************************************
    {
        if (!isComputed || getModel() != null)
            return;

        BendersModel model = new BendersModel();
        model.setBenderName(AndroidUtil.getString(R.string.predictive_offset_screen_name));
        model.setBendType(Bends.PREDICTIVE_OFFSET.getValue());

        HashMap<String,String> inputMap = new HashMap<>(2);
        inputMap.put(INPUT_FIELDKEY_ANGLE,mLabelAngle.getText().toString());
        inputMap.put(INPUT_FIELDKEY_OFFSET,mLabelOffset.getText().toString());
        model.setInputMap(inputMap);

        HashMap<String,String>outputMap = new HashMap<>(2);
        outputMap.put(OUTPUT_FIELD_MEASURE, mValueMeasure.getText().toString());
        outputMap.put(OUTPUT_FIELD_RUN, mValueRun.getText().toString());
        model.setOutputMap(outputMap);
        model.saveModel();
        ConnectionTask task = new ConnectionTask(this,ConnectionTask.OT_INSERT_BENDS);
        task.execute(model);

    }

    /**
     * @param angle
     * @param benderRadius 2*Bender Radius*TAN(RADIANS(Bend Angle/2))-(Bend Angle/360)*2*PI()*Radius)
     * @return
     */
    // ******************************************************************
    private double getCalculatedGain(double angle, double benderRadius)
    // ******************************************************************
    {
        //2*Bender Radius*TAN(RADIANS(Bend Angle/2))-(Bend Angle/360)*2*PI()*Radius)
        return Utils.round(((2 * benderRadius * Math.tan((Math.toRadians((angle / 2)))) - (angle / 360) * 2 * Math.PI * benderRadius)), 2);
    }

    /**
     * @param offset
     * @param bendAngle Calculated Offset = (1/(SIN(RADIANS(Bend Angle)))*Offset)
     * @return
     */
    // ******************************************************************
    private double getCalculatedOffset(double offset, double bendAngle)
    // ******************************************************************
    {
        //Formula E7*TAN(RADIANS(90-E4))
        double convertedAngle = Math.sin(Math.toRadians(bendAngle));
        return Utils.round(((1 / convertedAngle) * offset), 2);
    }

    /**
     * @param calculatedGain
     * @param calculatedOffset Measure To = Calculated Offset - Calculated Gain
     * @return
     */
    // ******************************************************************
    private double getMeasureTo(double calculatedGain, double calculatedOffset)
    // ******************************************************************
    {
        return Math.round(calculatedOffset - calculatedGain);
    }


    /**
     * @param offset
     * @param angle  Offset*TAN(RADIANS(90-Angle))
     */
    // ******************************************************************
    private double getRun(double offset, double angle)
    // ******************************************************************
    {
        //Run = Offset*TAN(RADIANS(90-Angle))
        return Utils.round(Math.round(offset * Math.tan(Math.toRadians(90 - angle))), 1);
    }
    //*********************************************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    //*********************************************************************
    {
        // Inflate the menu; this adds items to the action bar if it is pre
        getMenuInflater().inflate(R.menu.menu_info, menu);
        return true;
    }
    // ******************************************************************
    @Override
    protected void InfoUrl()
    // ******************************************************************
    {
        val url = getTutorialPath(HandTuberApplication.instance()
                                                      .getBendInfoByName(AndroidUtil.getString(
                                                              R.string.predictive_offset_screen_name))
                                                      .getTutorialPath());
        Intent infoTutorialIntent = new Intent(this, InfoTutorialActivity.class);
        infoTutorialIntent.putExtra(InfoTutorialActivity.INFO_TITLE,
                                    AndroidUtil.getString(R.string.predictive_offset_screen_name));
        infoTutorialIntent.putExtra(InfoTutorialActivity.SCREEN_URL, url);
        startActivity(infoTutorialIntent);
    }
}
