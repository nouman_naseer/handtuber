package abraham.graham.com.handtuber.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.HashMap;

import abraham.graham.com.handtuber.HandTuberApplication;
import abraham.graham.com.handtuber.R;
import abraham.graham.com.handtuber.Util.AndroidUtil;
import abraham.graham.com.handtuber.Util.Bends;
import abraham.graham.com.handtuber.Util.Utils;
import abraham.graham.com.handtuber.db.ConnectionTask;
import abraham.graham.com.handtuber.models.BendersModel;
import lombok.val;

//*********************************************************************
public class BackbendOffsetActivity
        extends BaseActivity
//*********************************************************************
{
    public static final String TAG = "BACK_BEND_OFFSET";
    private TextView mLabelDiagramRefLength;
    private TextView mLabelDiagramMeasureTo;
    private TextView mLabelDiagramAngle;

    private TextView mLabelValueMeasureTo;
    private EditText mAngle;
    private EditText mRefLength;
    private Button mBtnCalculate;

    /**
     * Input Fields
     */
    public static final String INPUT_FIELD_ANGLE = AndroidUtil.getString(R.string.label_angle);
    public static final String INPUT_FIELD_REF_LENGTH = AndroidUtil.getString(
            R.string.label_ref_length);

    /**
     * Output Fields
     */
    public static final String OUTPUT_FIELD_MEASURE = AndroidUtil.getString(
            R.string.label_measure_to);

    //*********************************************************************
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    //*********************************************************************
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.back_bend_offset_layout);
        val toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initControls();
    }

    //*********************************************************************
    private void initControls()
    //*********************************************************************
    {
        mLabelDiagramRefLength = (TextView) findViewById(R.id.txt_diagram_ref_length);
        mLabelDiagramMeasureTo = (TextView) findViewById(R.id.txt_diagram_measure);
        mLabelDiagramAngle = (TextView) findViewById(R.id.txt_diagram_angle);
        mLabelValueMeasureTo = (TextView) findViewById(R.id.label_measurement_value);

        mAngle = (EditText) findViewById(R.id.input_angle);
        mRefLength = (EditText) findViewById(R.id.input_ref_length);
        mBtnCalculate = (Button) findViewById(R.id.btn_cal);
        mBtnCalculate.setOnClickListener(new View.OnClickListener()
        {
            // ******************************************************************
            @Override
            public void onClick(View view)
            // ******************************************************************
            {
                computeValues();
            }
        });
        populateData();
    }

    // ******************************************************************
    private void computeValues()
    // ******************************************************************
    {
        if (TextUtils.isEmpty(mAngle.getText()
                                    .toString()))
            return;
        if (TextUtils.isEmpty(mRefLength.getText()
                                        .toString()))
            return;


        //Utils.getBenderRadius();
        double bendAngle = Double.valueOf(mAngle.getText()
                                                .toString());
        double refLength = Double.valueOf(mRefLength.getText()
                                                    .toString());
        mLabelDiagramRefLength.setText(mRefLength.getText()
                                                 .toString());
        mLabelDiagramAngle.setText(mAngle.getText()
                                         .toString());

        double reduction = getReduction(Utils.getBenderRadius(), bendAngle);
        double measureTo = refLength - reduction;
        mLabelDiagramMeasureTo.setText(Double.toString(measureTo));
        mLabelValueMeasureTo.setText(Double.toString(measureTo));
        isComputed = true;
    }

    // ******************************************************************
    private void populateData()
    // ******************************************************************
    {
        BendersModel model = getModel();
        if (model == null)
            return;

        model.setIOValues();
        mAngle.setText(model.getInputMap()
                            .get(INPUT_FIELD_ANGLE));
        mLabelDiagramAngle.setText(model.getInputMap()
                                        .get(INPUT_FIELD_ANGLE));
        mRefLength.setText(model.getInputMap()
                                .get(INPUT_FIELD_REF_LENGTH));
        mLabelDiagramRefLength.setText(model.getInputMap()
                                            .get(INPUT_FIELD_REF_LENGTH));

        mLabelDiagramMeasureTo.setText(model.getOutputMap()
                                            .get(OUTPUT_FIELD_MEASURE));
        mLabelValueMeasureTo.setText(model.getOutputMap()
                                          .get(OUTPUT_FIELD_MEASURE));
    }


    // ******************************************************************
    @Override
    protected void saveData()
    // ******************************************************************
    {
        if (!isComputed || getModel() != null)
            return;

        BendersModel model = new BendersModel();
        model.setBenderName(AndroidUtil.getString(R.string.back_bend_screen_name));
        model.setBendType(Bends.BACKBEND_OFFSET.getValue());

        HashMap<String, String> inputMap = new HashMap<>(2);
        inputMap.put(INPUT_FIELD_ANGLE, mAngle.getText()
                                              .toString());
        inputMap.put(INPUT_FIELD_REF_LENGTH, mRefLength.getText()
                                                       .toString());
        model.setInputMap(inputMap);

        HashMap<String, String> outputMap = new HashMap<>(2);
        outputMap.put(AndroidUtil.getString(R.string.label_measure_to),
                      mLabelDiagramMeasureTo.getText()
                                            .toString());
        model.setOutputMap(outputMap);
        model.saveModel();
        ConnectionTask task = new ConnectionTask(this, ConnectionTask.OT_INSERT_BENDS);
        task.execute(model);

    }

    /**
     * @param radius
     * @param angle
     * @return Reduction = Radius*TAN(RADIANS(Bend Angle/2))
     */
    // ******************************************************************
    private double getReduction(double radius, double angle)
    // ******************************************************************
    {

        return Math.round((radius * Math.tan(Math.toRadians(angle / 2))));
    }

    //*********************************************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    //*********************************************************************
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_info, menu);
        return true;
    }

    // ******************************************************************
    @Override
    protected void InfoUrl()
    // ******************************************************************
    {
        val url = getTutorialPath(HandTuberApplication.instance()
                                                      .getBendInfoByName(AndroidUtil.getString(
                                                              R.string.back_bend_screen_name))
                                                      .getTutorialPath());
        Intent infoTutorialIntent = new Intent(this, InfoTutorialActivity.class);
        infoTutorialIntent.putExtra(InfoTutorialActivity.INFO_TITLE,
                                    AndroidUtil.getString(R.string.back_bend_screen_name));
        infoTutorialIntent.putExtra(InfoTutorialActivity.SCREEN_URL, url);
        startActivity(infoTutorialIntent);
    }

}
