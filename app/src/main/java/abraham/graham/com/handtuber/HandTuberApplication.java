package abraham.graham.com.handtuber;

import android.app.Application;
import android.util.Log;

import java.util.ArrayList;

import abraham.graham.com.handtuber.Util.AndroidUtil;
import abraham.graham.com.handtuber.Util.Utils;
import abraham.graham.com.handtuber.models.BendsModel;
import lombok.NonNull;

/**
 * Created by root on 11/14/16.
 */

// ******************************************************************
public class HandTuberApplication extends Application
// ******************************************************************
{
    public static final String TAG = "HANDTUBBERAPP";

    private ArrayList<BendsModel> bendInfoArrayList;

    // ******************************************************************
    @Override
    public void onCreate()
    // ******************************************************************
    {
        super.onCreate();
        Log.d(TAG, "creating...");
        AndroidUtil.setContext(this);
    }
    //*********************************************************************
    public static @NonNull HandTuberApplication instance()
    //*********************************************************************
    {
        return (HandTuberApplication) AndroidUtil.getApplicationContext();
    }

    //*********************************************************************
    public ArrayList<BendsModel> getBendInfoList()
    //*********************************************************************
    {
        if(bendInfoArrayList == null)
        {
            //read the map information here.
            bendInfoArrayList = initBendData();

        }
        return bendInfoArrayList;
    }

    //*********************************************************************
    public BendsModel getBendInfoByName(String name)
    //*********************************************************************
    {
        BendsModel bendInfo = null;
        if(bendInfoArrayList == null)
            bendInfoArrayList = initBendData();
        for (BendsModel info:bendInfoArrayList)
        {
            if(info.getBendName().equalsIgnoreCase(name))
                bendInfo= info;
        }
        return bendInfo;
    }
    //*********************************************************************
    private ArrayList<BendsModel> initBendData()
    //*********************************************************************
    {
        ArrayList<BendsModel> bendList = new ArrayList<>();
        String[] bendsName = AndroidUtil.getStringArray(R.array.bends);
        String[] bendTutorialRes = AndroidUtil.getStringArray(R.array.tutorial_bends_resources);
        String[] bendRes = AndroidUtil.getStringArray(R.array.bends_resources);
        String[] mBendTutorialUrls = AndroidUtil.getStringArray(R.array.tutorial_urls);
        for (int i = 0; i < bendsName.length; i++)
        {
            BendsModel bendInfo = new BendsModel();
            bendInfo.setBendResourceId(Utils.getResourceId(bendRes[i], this));
            bendInfo.setBendName(bendsName[i]);
            bendInfo.setTutorialPath(mBendTutorialUrls[i]);
            bendInfo.setTutorialResourceId(Utils.getResourceId(bendTutorialRes[i], this));
            bendList.add(bendInfo);
        }
        return bendList;
    }
}
