package abraham.graham.com.handtuber.Util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.util.Log;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;

import abraham.graham.com.handtuber.R;

//*********************************************************************
public class Utils
//*********************************************************************
{


    /*
        Calculate BenderRadius, by making call to the DB.
     */
    //*********************************************************************
    public static double getBenderRadius()
    //*********************************************************************
    {
        return 38.1000;
    }



    //*********************************************************************
    public static void setDefaultUnit(int unit, Context context)
    //*********************************************************************
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(AndroidUtil.getString(R.string.shared_preferences), context.MODE_PRIVATE);
        sharedPreferences.edit().putInt(AndroidUtil.getString(R.string.default_unit_label), unit).commit();

    }

    //*********************************************************************
    public static int getDefaultUnit(Context context)
    //*********************************************************************
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(AndroidUtil.getString(R.string.shared_preferences), context.MODE_PRIVATE);

        return sharedPreferences.getInt(AndroidUtil.getString(R.string.default_unit_label), AndroidUtil.getResources().getInteger(R.integer.metric));
    }

    //*********************************************************************
    public static double round(double value, int places)
    //*********************************************************************
    {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    //*********************************************************************
    public static int getResourceId(Bends bend, Context context)
    //*********************************************************************
    {
        String[] bendsName = AndroidUtil.getStringArray(R.array.bends);
        String[] bendRes = AndroidUtil.getStringArray(R.array.bends_resources);
        int resId = -1;
        for (int i = 0; i < bendsName.length; i++)
        {
            if (bendsName[i].equalsIgnoreCase(bend.name())) {
                resId = AndroidUtil.getResources().getIdentifier(bendRes[i], "mipmap", context.getPackageName());
                break;
            }
        }

        return resId;
    }

    //*********************************************************************
    public static int getResourceId(int type, Context context)
    //*********************************************************************
    {
        String[] bendsName = AndroidUtil.getStringArray(R.array.bends);
        String[] bendRes = AndroidUtil.getStringArray(R.array.bends_resources);
        int resId = -1;
        for (int i = 0; i < bendsName.length; i++)
        {
            if (i == type)
            {
                resId = AndroidUtil.getResources().getIdentifier(bendRes[i], "mipmap", context.getPackageName());
                break;
            }
        }

        return resId;
    }
    //*********************************************************************
    public static int getResourceId(String bend, Context context)
    //*********************************************************************
    {
        return context.getResources().getIdentifier(bend, "mipmap", context.getPackageName());
    }



    // ******************************************************************
    public static @NonNull
    Uri getContentUri(@NonNull File file, @NonNull Context context)
    // ******************************************************************
    {
        return FileProvider.getUriForFile(context,
                                          AndroidUtil.getApplicationId() + ".files",
                                          file);
    }

}
