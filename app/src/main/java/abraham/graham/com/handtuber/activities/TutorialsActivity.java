package abraham.graham.com.handtuber.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import abraham.graham.com.handtuber.HandTuberApplication;
import abraham.graham.com.handtuber.R;
import abraham.graham.com.handtuber.adapters.BendAdapter;
import abraham.graham.com.handtuber.models.BendsModel;
import lombok.val;

/**
 * Created by nouman on 2/28/17.
 */
//*********************************************************************
public class TutorialsActivity extends  BaseActivity
//*********************************************************************
{

    private ListView mBendsListView;
    private ArrayList<BendsModel> mBendList;

    //*********************************************************************
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    //*********************************************************************
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tutorial_list_layout);
        val toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initControls();
    }


    //*********************************************************************
    private void initControls()
    //*********************************************************************
    {
        mBendList = HandTuberApplication.instance().getBendInfoList();
        mBendsListView = (ListView)findViewById(R.id.bend_list);
        mBendsListView.setAdapter(new BendAdapter(this, mBendList));
        mBendsListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {

            //*********************************************************************
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l)
            //*********************************************************************
            {

                    String url = getTutorialPath(mBendList.get(position).getTutorialPath());
                    Intent infoTutorialIntent = new Intent(TutorialsActivity.this,
                                                           InfoTutorialActivity.class);
                    infoTutorialIntent.putExtra(InfoTutorialActivity.INFO_TITLE,
                                                mBendList.get(position)
                                                         .getBendName());
                    infoTutorialIntent.putExtra(InfoTutorialActivity.SCREEN_URL, url);
                    startActivity(infoTutorialIntent);
            }
        });
    }


}
