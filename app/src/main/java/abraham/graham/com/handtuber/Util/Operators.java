package abraham.graham.com.handtuber.Util;

//*********************************************************************
public interface Operators
//*********************************************************************
{

    public static final String NONE = "none";
    public static final String CLEAR = "C";
    public static final String DIVIDER = "÷";
    public static final String MULTIPLICATION = "x";
    public static final String DELETE = "DEL";
    public static final String SUBTRACTION = "-";
    public static final String SUM = "+";
    public static final String EQUAL = "=";
    public static final String SUBMIT = ">";

    public static final String[] ARITHMETIC_OPERATORS = {"+", "-", "x", "÷"};
}
