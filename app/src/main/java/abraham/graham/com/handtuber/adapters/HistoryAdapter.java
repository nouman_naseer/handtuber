package abraham.graham.com.handtuber.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import abraham.graham.com.handtuber.R;
import abraham.graham.com.handtuber.Util.AndroidUtil;
import abraham.graham.com.handtuber.Util.Utils;
import abraham.graham.com.handtuber.db.DBConstants;

//*********************************************************************
public class HistoryAdapter extends android.widget.CursorAdapter
//*********************************************************************
{
    //*********************************************************************
    public HistoryAdapter(Context context,Cursor cursor)
    //*********************************************************************
    {
        super(context,cursor,0);
    }

    //*********************************************************************
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent)
    //*********************************************************************
    {
        return LayoutInflater.from(context).inflate(R.layout.bends_row_layout, parent, false);
    }

    //*********************************************************************
    @Override
    public void bindView(View view, Context context, Cursor cursor)
    //*********************************************************************
    {
        TextView txtName = (TextView) view.findViewById(R.id.bend_name);
        ImageView img =(ImageView)view.findViewById(R.id.bend_image);

        String name = cursor.getString(cursor.getColumnIndex(DBConstants.BENDERS_COL_NAME));
        int type = cursor.getInt(cursor.getColumnIndex(DBConstants.BENDERS_COL_TYPE));
        txtName.setText(name);
        img.setImageDrawable(AndroidUtil.getDrawable(Utils.getResourceId(type,context)));
    }
}
