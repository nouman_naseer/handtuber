package abraham.graham.com.handtuber.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.HashMap;

import abraham.graham.com.handtuber.HandTuberApplication;
import abraham.graham.com.handtuber.R;
import abraham.graham.com.handtuber.Util.AndroidUtil;
import abraham.graham.com.handtuber.Util.Bends;
import abraham.graham.com.handtuber.Util.Utils;
import abraham.graham.com.handtuber.db.ConnectionTask;
import abraham.graham.com.handtuber.models.BendersModel;
import lombok.val;

//*********************************************************************
public class LongRunOffsetActivity extends BaseActivity
//*********************************************************************
{
    private EditText mRunLength;
    private EditText mOffset;

    private TextView mSlopeAngleValue;
    private TextView mSlopeLengthValue;
    private TextView mAdjAngleValue;

    private TextView mLabelAngle;
    private TextView mLabelRun;
    private TextView mLabelOffset;
    private TextView mLabelSlope;
    Button mCalculate;

    public static String INPUT_FIELDKEY_OFFSET=AndroidUtil.getString(R.string.label_offset);
    public static String INPUT_FIELDKEY_RUNLENGTH=AndroidUtil.getString(R.string.label_run_length);

    public static final String OUTPUT_FIELD_SLOPE_ANGLE = AndroidUtil.getString(R.string.label_slope_angle);
    public static final String OUTPUT_FIELD_SLOPE_LENGTH=AndroidUtil.getString(R.string.label_slope_length);
    public static final String OUTPUT_FIELD_ADJ_ANGLE=AndroidUtil.getString(R.string.adj_angle);
    //*********************************************************************
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    //*********************************************************************
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.long_run_offset_layout);
        val toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initControls();
    }

    // ******************************************************************
    private void initControls()
    // ******************************************************************
    {
        mRunLength = (EditText) findViewById(R.id.input_run_length);
        mOffset = (EditText) findViewById(R.id.input_offset);
        mCalculate = (Button) findViewById(R.id.btn_cal);
        mCalculate.setOnClickListener(new View.OnClickListener() {
            // ******************************************************************
            @Override
            public void onClick(View view)
            // ******************************************************************
            {
                computeValues();
            }
        });
        mSlopeAngleValue =(TextView)findViewById(R.id.label_slope_angle_value);
        mSlopeLengthValue =(TextView)findViewById(R.id.label_slope_length_value);
        mAdjAngleValue =(TextView)findViewById(R.id.label_adj_angle_value);
        mLabelAngle = (TextView)findViewById(R.id.txt_diagram_angle);
        mLabelRun = (TextView)findViewById(R.id.txt_diagram_run);
        mLabelOffset = (TextView)findViewById(R.id.txt_diagram_offset);
        mLabelSlope = (TextView)findViewById(R.id.txt_diagram_slope);
        populateData();
    }


    // ******************************************************************
    private void computeValues()
    // ******************************************************************
    {
        if(TextUtils.isEmpty(mRunLength.getText().toString()))
            return;

        if(TextUtils.isEmpty(mOffset.getText().toString()))
            return;

        double runLength = Double.parseDouble(mRunLength.getText().toString());
        double offset = Double.parseDouble(mOffset.getText().toString());


        mLabelRun.setText(mRunLength.getText().toString());
        mLabelOffset.setText(mOffset.getText().toString());

        double slope = getSlope(offset, runLength);
        String slopeString = Double.toString(slope);
        mLabelSlope.setText(slopeString);
        mSlopeLengthValue.setText(slopeString);
        double slopeAngle = getSlopeAngle(offset,runLength);
        String slopeAngleString = Double.toString(slopeAngle);
        mSlopeAngleValue.setText(slopeAngleString);
        mLabelAngle.setText(slopeAngleString);

        double adjAngle = getAdjAngle(slopeAngle);
        mAdjAngleValue.setText(Double.toString(adjAngle));
        isComputed = true;
    }


    // ******************************************************************
    private void populateData()
    // ******************************************************************
    {
        BendersModel model = getModel();
        if (model == null)
            return;

        model.setIOValues();
        mRunLength.setText(model.getInputMap().get(INPUT_FIELDKEY_RUNLENGTH));
        mOffset.setText(model.getInputMap().get(INPUT_FIELDKEY_OFFSET));

        mSlopeAngleValue.setText(model.getOutputMap().get(OUTPUT_FIELD_SLOPE_ANGLE));
        mLabelAngle.setText(model.getOutputMap().get(OUTPUT_FIELD_SLOPE_ANGLE));

        mSlopeLengthValue.setText(model.getOutputMap().get(OUTPUT_FIELD_SLOPE_LENGTH));
        mLabelSlope.setText(model.getOutputMap().get(OUTPUT_FIELD_SLOPE_LENGTH));

        mAdjAngleValue.setText(model.getOutputMap().get(OUTPUT_FIELD_ADJ_ANGLE));


    }


    // ******************************************************************
    @Override
    protected void saveData()
    // ******************************************************************
    {
        if(!isComputed || getModel() != null)
            return;

        BendersModel model = new BendersModel();
        model.setBenderName(AndroidUtil.getString(R.string.longrun_offset_screen_name));
        model.setBendType(Bends.LONG_RUN_OFFSET.getValue());

        HashMap<String,String> inputMap = new HashMap<>(2);
        inputMap.put(INPUT_FIELDKEY_RUNLENGTH,mLabelRun.getText().toString());
        inputMap.put(INPUT_FIELDKEY_OFFSET,mLabelOffset.getText().toString());
        model.setInputMap(inputMap);

        HashMap<String,String>outputMap = new HashMap<>(3);
        outputMap.put(OUTPUT_FIELD_SLOPE_ANGLE, mSlopeAngleValue.getText().toString());
        outputMap.put(OUTPUT_FIELD_SLOPE_LENGTH, mSlopeLengthValue.getText().toString());
        outputMap.put(OUTPUT_FIELD_ADJ_ANGLE, mAdjAngleValue.getText().toString());

        model.setOutputMap(outputMap);
        model.saveModel();
        ConnectionTask task = new ConnectionTask(this,ConnectionTask.OT_INSERT_BENDS);
        task.execute(model);

    }

    /**
     * @param  offset
     * @param length
     * @return Slope = √(Offset2 + Run2)
     */
    // ******************************************************************
    private double getSlope(double offset,double length)
    // ******************************************************************
    {
        //Slope = √(Offset2 + Run2)
        double offsetPower = Math.pow(offset,2);
        double lengthPower = Math.pow(length,2);
        return Utils.round(Math.sqrt(offsetPower + lengthPower), 2);
    }

    /**
     *
     * @param offset
     * @param length
     * @return  //Angle = DEGREES(ATAN(Offset/Run))
     */
    // ******************************************************************
    private double getSlopeAngle(double offset,double length)
    // ******************************************************************
    {
        //Angle = DEGREES(ATAN(Offset/Run))
        double arcAngle = Math.atan(offset/length);
        return Utils.round(Math.toDegrees(arcAngle),1);
    }

    // ******************************************************************
    private double getAdjAngle(double angle)
    // ******************************************************************
    {
        return Utils.round((90 - angle),1);
    }
    //*********************************************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    //*********************************************************************
    {
        // Inflate the menu; this adds items to the action bar if it is pre
        getMenuInflater().inflate(R.menu.menu_info, menu);
        return true;
    }
    // ******************************************************************
    @Override
    protected void InfoUrl()
    // ******************************************************************
    {
        val url = getTutorialPath(HandTuberApplication.instance()
                                                      .getBendInfoByName(AndroidUtil.getString(
                                                              R.string.longrun_offset_screen_name))
                                                      .getTutorialPath());
        Intent infoTutorialIntent = new Intent(this, InfoTutorialActivity.class);
        infoTutorialIntent.putExtra(InfoTutorialActivity.INFO_TITLE,
                                    AndroidUtil.getString(R.string.longrun_offset_screen_name));
        infoTutorialIntent.putExtra(InfoTutorialActivity.SCREEN_URL, url);
        startActivity(infoTutorialIntent);
    }
}
