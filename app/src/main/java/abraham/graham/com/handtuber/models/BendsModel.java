package abraham.graham.com.handtuber.models;

import lombok.Getter;
import lombok.Setter;


//*********************************************************************
public class BendsModel
//*********************************************************************
{
    @Getter @Setter String bendName;
    @Getter @Setter int bendResourceId;
    @Setter @Getter int tutorialResourceId;
    @Setter @Getter String tutorialPath;

    //*********************************************************************
    public BendsModel(String name, int resource)
    //*********************************************************************
    {
        this.bendName = name;
        this.bendResourceId = resource;
    }

    //*********************************************************************
    public BendsModel()
    //*********************************************************************
    {

    }

}
