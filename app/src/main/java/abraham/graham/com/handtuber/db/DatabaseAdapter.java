package abraham.graham.com.handtuber.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import abraham.graham.com.handtuber.models.BendersModel;
import abraham.graham.com.handtuber.models.Bends;

//*********************************************************************
public class DatabaseAdapter
//*********************************************************************
{
    private Context mContext;
    private SQLiteDatabase mDatabase;
    private DatabaseHelper mDBHandler;

    //*********************************************************************
    public DatabaseAdapter(Context context)
    //*********************************************************************
    {
        mContext = context;

    }
    //*********************************************************************
    public DatabaseAdapter open() throws SQLException
    //*********************************************************************
    {
        mDBHandler = new DatabaseHelper(mContext);
        mDatabase = mDBHandler.getWritableDatabase();
        return this;
    }

    //*********************************************************************
    public void close()
    //*********************************************************************
    {
        mDBHandler.close();
    }

    //*********************************************************************
    public long insertBender(BendersModel model)
    //*********************************************************************
    {
        long returnValue = -1;
        try
        {
            ContentValues values = new ContentValues();
            values.put(DBConstants.BENDERS_COL_NAME, model.getBenderName());
            values.put(DBConstants.BENDERS_COL_TYPE, model.getBendType());
            values.put(DBConstants.BENDERS_COL_INPUT_VALUE, model.getInputValues());
            values.put(DBConstants.BENDERS_COL_OUTPUT_VALUE, model.getOutputValues());
            returnValue = mDatabase.insertWithOnConflict(DBConstants.BENDERS_TABLE,
                    null, values, SQLiteDatabase.CONFLICT_IGNORE);
        }
        catch (Exception e)
        {
            System.out.println();
        }
        return returnValue;
    }

    //*********************************************************************
    public long insertBend(Bends model)
    //*********************************************************************
    {
        long returnValue = -1;
        try
        {
            ContentValues values = new ContentValues();
            values.put(DBConstants.BENDS_COL_NAME, model.getName());
            values.put(DBConstants.BENDS_COL_TYPE, model.getBendType());
            values.put(DBConstants.BENDS_COL_RADIUS, model.getRadius());
            values.put(DBConstants.BENDS_COL_SIZE, model.getSize());
            values.put(DBConstants.BENDS_COL_UNIT, model.getMeasureUnit());
            values.put(DBConstants.BENDS_COL_PATH,model.getFilePath());
            values.put(DBConstants.BENDS_COL_DEFAULT,model.getDefaultBender());
            returnValue = mDatabase.insertWithOnConflict(DBConstants.BENDS_TABLE,
                    null, values, SQLiteDatabase.CONFLICT_IGNORE);
        }
        catch (Exception e)
        {
            System.out.println();
        }
        return returnValue;
    }

    //*********************************************************************
    public long updateBends(Bends model)
    //*********************************************************************
    {
        long returnValue = -1;
        try
        {
            ContentValues values = new ContentValues();
            values.put("_id",model.get_id());
            values.put(DBConstants.BENDS_COL_NAME, model.getName());
            values.put(DBConstants.BENDS_COL_TYPE, model.getBendType());
            values.put(DBConstants.BENDS_COL_RADIUS, model.getRadius());
            values.put(DBConstants.BENDS_COL_SIZE, model.getSize());
            values.put(DBConstants.BENDS_COL_UNIT, model.getMeasureUnit());
            values.put(DBConstants.BENDS_COL_PATH,model.getFilePath());
            values.put(DBConstants.BENDS_COL_DEFAULT,model.getDefaultBender());
            returnValue = mDatabase.update(DBConstants.BENDS_TABLE,values,"_id="+model.get_id(),null);
        }
        catch (Exception e)
        {
            System.out.println();
        }
        return returnValue;
    }

    /**
     *
     * @return
     * public Cursor query (String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit)
     */
    //*********************************************************************
    public Cursor fetchBenders()
    //*********************************************************************
    {
        return mDatabase.query(DBConstants.BENDERS_TABLE, new String[]{"_id", DBConstants.BENDERS_COL_NAME,
                DBConstants.BENDERS_COL_TYPE, DBConstants.BENDERS_COL_INPUT_VALUE, DBConstants.BENDERS_COL_OUTPUT_VALUE}, null, null, null, null, "_id DESC", DBConstants.BEND_FETCH_LIMIT);
    }
    /**
     *
     * @return
     *  values.put(DBConstants.BENDS_COL_NAME, model.getName());
    values.put(DBConstants.BENDS_COL_TYPE, model.getBendType());
    values.put(DBConstants.BENDS_COL_RADIUS, model.getRadius());
    values.put(DBConstants.BENDS_COL_SIZE, model.getSize());
    values.put(DBConstants.BENDS_COL_UNIT, model.getMeasureUnit());
     */

    /*
    public Cursor query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        throw new RuntimeException("Stub!");
    }
     */
    //*********************************************************************
    public Cursor fetchBends()
    //*********************************************************************
    {
        return mDatabase.query(DBConstants.BENDS_TABLE, new String[]{"_id", DBConstants.BENDS_COL_NAME,
                DBConstants.BENDS_COL_TYPE, DBConstants.BENDS_COL_RADIUS, DBConstants.BENDS_COL_SIZE, DBConstants.BENDS_COL_UNIT,DBConstants.BENDS_COL_PATH,DBConstants.BENDS_COL_DEFAULT}, DBConstants.BENDS_COL_TYPE, null, null, null, "_id DESC", null);
    }

    //*********************************************************************
    public Cursor fetchDefaultBend()
    //*********************************************************************
    {
        return mDatabase.rawQuery("select * from "+DBConstants.BENDS_TABLE+" where "+DBConstants.BENDS_COL_DEFAULT+" = ?", new String[] {1+""});
    }

}
