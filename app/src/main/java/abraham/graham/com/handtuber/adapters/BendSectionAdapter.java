package abraham.graham.com.handtuber.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

import abraham.graham.com.handtuber.R;
import abraham.graham.com.handtuber.Util.AndroidUtil;
import abraham.graham.com.handtuber.models.Bends;

/**
 * Created by nouman on 4/4/17.
 */

//*********************************************************************
public class BendSectionAdapter  extends BaseAdapter
//*********************************************************************
{

    private static final int TYPE_BEND = 0;
    private static final int TYPE_SECTION = TYPE_BEND + 1;
    private ArrayList<Bends> mBendList;
    private LayoutInflater mLayoutInflater;
    private Context mContext;

    //*********************************************************************
    public BendSectionAdapter(ArrayList<Bends> list, LayoutInflater inflater, Context context)
    //*********************************************************************
    {
        mBendList = list;
        mLayoutInflater = inflater;
        mContext = context;
    }

    //*********************************************************************
    @Override
    public int getViewTypeCount()
    //*********************************************************************
    {
        return 2;
    }

    //*********************************************************************
    @Override
    public int getCount()
    //*********************************************************************
    {
        return mBendList.size();
    }

    //*********************************************************************
    @Override
    public Object getItem(int position)
    //*********************************************************************
    {
        return mBendList.get(position);
    }

    //*********************************************************************
    @Override
    public long getItemId(int position)
    //*********************************************************************
    {
        return 0;
    }


    //*********************************************************************
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    //*********************************************************************
    {
        BendViewHolder holder = null;
        int rowType = getItemViewType(position);

        if (convertView == null)
        {
            holder = new BendViewHolder();
            switch (rowType)
            {
            case TYPE_BEND:
                convertView = mLayoutInflater.inflate(R.layout.bends_row_view, null);
                holder.bendImage = (ImageView) convertView.findViewById(R.id.benderImage);
                holder.bendName = (TextView) convertView.findViewById(R.id.txtBendName);
                holder.bendSize = (TextView) convertView.findViewById(R.id.txtSize);
                holder.bendRadius = (TextView) convertView.findViewById(R.id.txtRadius);
                holder.defaultBenderImage = (ImageView) convertView.findViewById(
                        R.id.defaultBenderImage);
                break;
            case TYPE_SECTION:
                convertView = mLayoutInflater.inflate(R.layout.seprator, null);
                holder.headerText = (TextView) convertView.findViewById(R.id.textSeparator);
                break;
            }
            convertView.setTag(holder);
        }
        else
        {
            holder = (BendViewHolder) convertView.getTag();
        }

        switch (rowType)
        {
        case TYPE_BEND:
            bindBendView(holder,mBendList.get(position));
            break;
        case TYPE_SECTION:
            bindSectionView(holder,mBendList.get(position));
            break;
        }
        return convertView;
    }

    //*********************************************************************
    private void bindSectionView(@NonNull BendViewHolder bendHolder, @NonNull Bends item)
    //*********************************************************************
    {
        bendHolder.headerText.setText(item.getSectionName());
    }

    //*********************************************************************
    private void bindBendView(@NonNull BendViewHolder bendHolder, @NonNull Bends item)
    //*********************************************************************
    {
        bendHolder.bendName.setText(item.getName());
        bendHolder.bendSize.setText(String.format(AndroidUtil.getString(R.string.bender_item_size), item.getSize()));
        bendHolder.bendRadius.setText(String.format(AndroidUtil.getString(R.string.bender_item_radius),item.getRadius()));
        if (item
                .getBendType() == Bends.BEND_APP_SPECIFIC)
        {
            //load the image from assets.
            Picasso.with(mContext).load(AndroidUtil.getAssetImagePath(item.getFilePath()))
                   .placeholder(AndroidUtil.getDrawable(R.mipmap.icon_bender_placeholder))
                   .into(bendHolder.bendImage);
        }
        else
        {
            //check if the file path empty, use default image.
            if (TextUtils.isEmpty(item.getFilePath()))
                Picasso.with(mContext).load(R.mipmap.icon_bender_placeholder)
                       .into(bendHolder.bendImage);
            else
            {
                File file = new File(item
                                             .getFilePath());
                Picasso.with(mContext)
                       .load(file)
                       .error(R.mipmap.icon_bender_placeholder)
                       .into(bendHolder.bendImage);
            }

        }

        if(item.getDefaultBender()==Bends.BEND_DEFAULT)
            bendHolder.defaultBenderImage.setVisibility(View.VISIBLE);
        else
            bendHolder.defaultBenderImage.setVisibility(View.INVISIBLE);
    }


    //*********************************************************************
    @Override
    public int getItemViewType(int position)
    //*********************************************************************
    {
        return mBendList.get(position)
                        .getBendType() == 0 ? TYPE_SECTION : TYPE_BEND;
    }

    //*********************************************************************
    public static class BendViewHolder
    //*********************************************************************
    {
        public ImageView bendImage;
        public TextView bendName;
        public TextView bendSize;
        public TextView bendRadius;
        public TextView headerText;
        public ImageView defaultBenderImage;
    }

}
