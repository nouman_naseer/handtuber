package abraham.graham.com.handtuber.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import abraham.graham.com.handtuber.HomeActivity;
import abraham.graham.com.handtuber.R;
import abraham.graham.com.handtuber.Util.AndroidUtil;
import abraham.graham.com.handtuber.Util.UIResult;
import abraham.graham.com.handtuber.db.ConnectionTask;


//*********************************************************************
public class SplashActivity
        extends BaseActivity
        implements UIResult
//*********************************************************************
{

    private final int SPLASH_TIME = 2000;
    private final int DELAY_TIME = 1 * 1000;
    private SharedPreferences mPrefs = null;

    //*********************************************************************
    @Override
    protected void onCreate(Bundle savedInstanceState)
    //*********************************************************************
    {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                             WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splash);
        initControls();
    }

    //*********************************************************************
    private void initControls()
    //*********************************************************************
    {
        mPrefs = getSharedPreferences(AndroidUtil.getString(R.string.shared_preferences),
                                      MODE_PRIVATE);
        if (mPrefs.getBoolean(AndroidUtil.getString(R.string.first_run), true))
        {
            Log.d("Splash", "First run...");

            // Do first run stuff here then set 'firstrun' as false
            // using the following line to edit/commit prefs
            ConnectionTask readFile = new ConnectionTask(this, ConnectionTask.OT_READ_BENDERS_FILE);
            readFile.execute("");
        }
        else
        {
            new Handler().postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                    finish();
                }
            }, DELAY_TIME);
        }
    }


    //*********************************************************************
    @Override
    public void updateResult(Object result)
    //*********************************************************************
    {
        //set first time run = false
        startActivity(new Intent(SplashActivity.this, HomeActivity.class));
        finish();

    }



}
