package abraham.graham.com.handtuber.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.HashMap;

import abraham.graham.com.handtuber.HandTuberApplication;
import abraham.graham.com.handtuber.R;
import abraham.graham.com.handtuber.Util.AndroidUtil;
import abraham.graham.com.handtuber.Util.Bends;
import abraham.graham.com.handtuber.Util.Utils;
import abraham.graham.com.handtuber.db.ConnectionTask;
import abraham.graham.com.handtuber.models.BendersModel;
import lombok.val;

//*********************************************************************
public class FallCalculator extends BaseActivity
//*********************************************************************
{

    TextView mLabelDiagramAngle;
    TextView mLabelDiagramDistance;
    TextView mLabelDiagramFall;
    TextView mLabelDiagramSlope;

    TextView mLabelValueFall;
    TextView mLabelValueSlope;

    EditText mDistance;
    EditText mRatio;

    Button mCal;

    private final String TAG = "FALL_CALCULATOR";

    /**
     * Input Fields
     */
    public static final String INPUT_FIELD_DISTANCE = AndroidUtil.getString(R.string.label_distance);
    public static final String INPUT_FIELD_RATIO = AndroidUtil.getString(R.string.label_ratio);

    /**
     * Output Fields
     */
    public static final String OUTPUT_FIELD_FALL = AndroidUtil.getString(R.string.label_fall);
    public static final String OUTPUT_FIELD_SLOPE_LENGTH = AndroidUtil.getString(R.string.label_slope_length);
    public static final String OUTPUT_FIELD_ANGLE=AndroidUtil.getString(R.string.label_angle);

    //*********************************************************************
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    //*********************************************************************
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fall_calculator);
        val toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initControls();
    }

    //*********************************************************************
    private void initControls()
    //*********************************************************************
    {
        mLabelDiagramAngle = (TextView) findViewById(R.id.txt_diagram_ref_length);
        mLabelDiagramDistance = (TextView) findViewById(R.id.txt_diagram_distance);
        mLabelDiagramFall = (TextView) findViewById(R.id.txt_diagram_fall);
        mLabelDiagramSlope = (TextView) findViewById(R.id.txt_diagram_slope);
        mLabelValueFall = (TextView) findViewById(R.id.label_fall_value);
        mLabelValueSlope = (TextView) findViewById(R.id.label_slope_length_value);

        mDistance = (EditText) findViewById(R.id.input_distance);
        mRatio = (EditText) findViewById(R.id.input_ratio_length);

        mCal = (Button) findViewById(R.id.btn_cal);
        mCal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                computeValues();
            }
        });
        populateData();
    }

    //*********************************************************************
    private void computeValues()
    //*********************************************************************
    {
        if (TextUtils.isEmpty(mDistance.getText().toString()))
            return;
        if (TextUtils.isEmpty(mRatio.getText().toString()))
            return;

        double distance = Double.parseDouble(mDistance.getText().toString());
        double ratio = Double.parseDouble(mRatio.getText().toString());
        double calculatedFall = getCalculatedFall(ratio);

        Log.d(TAG, "calculatedFall => " + calculatedFall);

        double angle = getCalculatedAngle(calculatedFall);
        Log.d(TAG, "angle => " + angle);
        double fall = getFall(calculatedFall, distance);

        Log.d(TAG, "fall => " + fall);
        double slope = getSlope(distance, angle);
        Log.d(TAG, "slope => " + slope);

        mLabelDiagramAngle.setText(Double.toString(angle));
        mLabelDiagramDistance.setText(mDistance.getText().toString());

        mLabelDiagramFall.setText(Double.toString(fall));
        mLabelValueFall.setText(Double.toString(fall));
        mLabelDiagramSlope.setText(Double.toString(slope));
        mLabelValueSlope.setText(Double.toString(slope));

        isComputed = true;
    }

    // ******************************************************************
    private void populateData()
    // ******************************************************************
    {
        BendersModel model = getModel();
        if (model == null)
            return;

        model.setIOValues();
        mDistance.setText(model.getInputMap().get(INPUT_FIELD_DISTANCE));
        mLabelDiagramDistance.setText(model.getInputMap().get(INPUT_FIELD_DISTANCE));
        mRatio.setText(model.getInputMap().get(INPUT_FIELD_RATIO));

        mLabelDiagramAngle.setText(model.getOutputMap().get(OUTPUT_FIELD_ANGLE));
        mLabelDiagramFall.setText(model.getOutputMap().get(OUTPUT_FIELD_FALL));
        mLabelValueFall.setText(model.getOutputMap().get(OUTPUT_FIELD_FALL));

        mLabelDiagramSlope.setText(model.getOutputMap().get(OUTPUT_FIELD_SLOPE_LENGTH));
        mLabelValueSlope.setText(model.getOutputMap().get(OUTPUT_FIELD_SLOPE_LENGTH));
    }


    // ******************************************************************
    @Override
    protected void saveData()
    // ******************************************************************
    {
        if(!isComputed || getModel() != null)
            return;

        BendersModel model = new BendersModel();
        model.setBenderName(AndroidUtil.getString(R.string.fall_calculator_screen_name));
        model.setBendType(Bends.FALL_CALCULATOR.getValue());

        HashMap<String,String> inputMap = new HashMap<>(2);
        inputMap.put(INPUT_FIELD_DISTANCE,mDistance.getText().toString());
        inputMap.put(INPUT_FIELD_RATIO,mRatio.getText().toString());
        model.setInputMap(inputMap);

        HashMap<String,String>outputMap = new HashMap<>(2);
        outputMap.put(OUTPUT_FIELD_FALL, mLabelValueFall.getText().toString());
        outputMap.put(OUTPUT_FIELD_SLOPE_LENGTH, mLabelValueSlope.getText().toString());
        outputMap.put(OUTPUT_FIELD_ANGLE, mLabelDiagramAngle.getText().toString());
        model.setOutputMap(outputMap);
        model.saveModel();
        ConnectionTask task = new ConnectionTask(this,ConnectionTask.OT_INSERT_BENDS);
        task.execute(model);

    }

    /**
     * @param ratio
     * @return
     */
    //*********************************************************************
    private double getCalculatedFall(double ratio)
    //*********************************************************************
    {
        return (100 / ratio);
    }

    /**
     * @param calculatedRatio
     * @return Angle = DEGREES(Fall %/100)
     */
    //*********************************************************************
    private double getCalculatedAngle(double calculatedRatio)
    //*********************************************************************
    {
        return Utils.round(Math.toDegrees(calculatedRatio / 100), 1);
    }

    /**
     * @param calculatedRatio * @param distance
     * @return (Distance*Fall %)/100
     */
    //*********************************************************************
    private double getFall(double calculatedRatio, double distance)
    //*********************************************************************
    {
        return Math.round(((distance * calculatedRatio) / 100));
    }

    /**
     * @param distance
     * @param angle
     * @return Distance/SIN(RADIANS(90-Angle))
     */
    //*********************************************************************
    private double getSlope(double distance, double angle)
    //*********************************************************************
    {
        return Math.round((distance / Math.sin(Math.toRadians(90 - angle))));
    }
    //*********************************************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    //*********************************************************************
    {
        // Inflate the menu; this adds items to the action bar if it is pre
        getMenuInflater().inflate(R.menu.menu_info, menu);
        return true;
    }
    // ******************************************************************
    @Override
    protected void InfoUrl()
    // ******************************************************************
    {
        val url = getTutorialPath(HandTuberApplication.instance()
                                                      .getBendInfoByName(AndroidUtil.getString(
                                                              R.string.fall_calculator_screen_name))
                                                      .getTutorialPath());
        Intent infoTutorialIntent = new Intent(this, InfoTutorialActivity.class);
        infoTutorialIntent.putExtra(InfoTutorialActivity.INFO_TITLE,
                                    AndroidUtil.getString(R.string.fall_calculator_screen_name));
        infoTutorialIntent.putExtra(InfoTutorialActivity.SCREEN_URL, url);
        startActivity(infoTutorialIntent);
    }
}
